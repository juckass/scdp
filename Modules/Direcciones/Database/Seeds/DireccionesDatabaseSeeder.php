<?php 

namespace Modules\Direcciones\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DireccionesDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call(estadosSeeder::class);
		$this->call(ciudadesSeeder::class);
		$this->call(municipiosSeeder::class);
		$this->call(parroquiasSeeder::class);

		Model::reguard();
	}
}
