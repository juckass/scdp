<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ubicacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('estados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100);
            $table->string('iso_3166-2',100);
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ciudades', function (Blueprint $table){
            $table->increments('id');
            $table->integer('estados_id')->unsigned()->nullable();

            $table->string('nombre',100);
            $table->tinyInteger('capital')->default(0);
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('estados_id')
                ->references('id')->on('estados')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('municipio', function (Blueprint $table){
            $table->increments('id');
            $table->integer('estados_id')->unsigned()->nullable();

            $table->string('nombre',100);
            $table->integer('poblacion')->unsigned()->nullable();
            $table->text('opciones')->nullable();
            $table->text('path')->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('estados_id')
                ->references('id')->on('estados')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('parroquia', function(Blueprint $table){
            $table->increments('id');
            
            $table->string('nombre', 100);
            $table->integer('municipio_id')->unsigned()->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('municipio_id')
                ->references('id')->on('municipio')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('sector', function(Blueprint $table){
            $table->increments('id');
            
            $table->string('nombre', 100);
            $table->string('slug', 100);
            $table->integer('parroquia_id')->unsigned()->nullable();

            $table->unique(['slug', 'parroquia_id'], 'sector_index_unique');
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('parroquia_id')
                ->references('id')->on('parroquia')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sector');
        Schema::dropIfExists('parroquia');
        Schema::dropIfExists('municipio');
        Schema::dropIfExists('ciudades');
        Schema::dropIfExists('estados');
    }
}
