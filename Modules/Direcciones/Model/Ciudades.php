<?php

namespace Modules\Direcciones\Model;

use Modules\Admin\Model\Modelo;

use Modules\Direcciones\Model\Estados;

class Ciudades extends modelo
{
    protected $table = 'ciudades';
    protected $fillable = ["estados_id","nombre","capital"];
    protected $campos = [
    'estados_id' => [
        'type' => 'select',
        'label' => 'Estados',
        'placeholder' => '- Seleccione un Estados',
        'url' => 'estados'
    ],
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Ciudades'
    ],
    'capital' => [
        'type' => 'checkbox',
        'label' => 'Capital',
        'placeholder' => 'Capital del Ciudades'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['estados_id']['options'] = Estados::pluck('nombre', 'id');
    }

    public function estados()
	{
		return $this->belongsTo('Modules\Direcciones\Model\Estados');
	}

	public function empresas(){
		return $this->hasMany('Modules\Panaderias\Model\Empresa', 'ciudades_id');
	}
}