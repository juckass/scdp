<?php

namespace Modules\Direcciones\Model;

use Modules\Admin\Model\Modelo;



class Estados extends modelo
{
    protected $table = 'estados';
    protected $fillable = ["nombre","iso_3166-2"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Estados'
    ],
    'iso_3166-2' => [
        'type' => 'text',
        'label' => 'Iso 3166-2',
        'placeholder' => 'Iso 3166-2 del Estados'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    public function municipio()
	{
		return $this->hasMany('Modules\Direcciones\Model\Municipio');
	}

    public function empresas(){
		return $this->hasMany('Modules\Panaderias\Model\Empresa', 'estados_id');
	}

	
}