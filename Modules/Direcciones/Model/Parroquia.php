<?php

namespace Modules\Direcciones\Model;

use Modules\Admin\Model\Modelo;

use Modules\Direcciones\Model\Municipio;

class Parroquia extends modelo
{
    protected $table = 'parroquia';
    protected $fillable = ["nombre","municipio_id"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Parroquia'
    ],
    'municipio_id' => [
        'type' => 'select',
        'label' => 'Municipio',
        'placeholder' => '- Seleccione un Municipio',
        'url' => 'municipio'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['municipio_id']['options'] = Municipio::pluck('nombre', 'id');
    }

    public function municipio()
	{
		return $this->belongsTo('Modules\Direcciones\Model\Municipio');
	}

	public function sector()
	{
		return $this->hasMany('Modules\Direcciones\Model\Sector');
	}
    
    public function empresas(){
		return $this->hasMany('Modules\Panaderias\Model\Empresa', 'parroquia_id');
	}

	
}