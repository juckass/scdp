<?php

namespace Modules\Direcciones\Model;

use Modules\Admin\Model\Modelo;

use Modules\Direcciones\Model\Estados;

class Municipio extends modelo
{
    protected $table = 'municipio';
    protected $fillable = ["estados_id","nombre","poblacion","opciones","path"];
    protected $campos = [
    'estados_id' => [
        'type' => 'select',
        'label' => 'Estados',
        'placeholder' => '- Seleccione un Estados',
        'url' => 'estados'
    ],
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Municipio'
    ],
    'poblacion' => [
        'type' => 'number',
        'label' => 'Poblacion',
        'placeholder' => 'Poblacion del Municipio'
    ],
    'opciones' => [
        'type' => 'textarea',
        'label' => 'Opciones',
        'placeholder' => 'Opciones del Municipio'
    ],
    'path' => [
        'type' => 'textarea',
        'label' => 'Path',
        'placeholder' => 'Path del Municipio'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['estados_id']['options'] = Estados::pluck('nombre', 'id');
    }

    public function estados()
	{
		return $this->belongsTo('Modules\Direcciones\Model\Estados');
	}

	public function parroquia()
	{
		return $this->hasMany('Modules\Direcciones\Model\Parroquia');
	}

    public function empresas(){
		return $this->hasMany('Modules\Panaderias\Model\Empresa', 'municipio_id');
	}

	
}