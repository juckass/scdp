<?php

namespace Modules\Direcciones\Model;

use Modules\Admin\Model\Modelo;

use Modules\Direcciones\Model\Parroquia;
use Modules\Direcciones\Model\Estados;

class Sector extends modelo
{
    protected $table = 'sector';
    protected $fillable = ["nombre","slug","parroquia_id"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Sector'
    ],
    'slug' => [
        'type' => 'hidden',
        'label' => 'Slug',
        'placeholder' => 'Slug del Sector'
    ],
    'estados_id' => [
        'type' => 'select',
        'label' => 'Estado',
        'placeholder' => '- Seleccione ...',
        'url' => 'Direcciones/definiciones/estados'
    ],
    'municipio_id' => [
        'type' => 'select',
        'label' => 'Municipio',
        'placeholder' => '- Seleccione ...',
        'url' => 'Direcciones/definiciones/municipio'
    ],
    'parroquia_id' => [
        'type' => 'select',
        'label' => 'Parroquia',
        'placeholder' => '- Seleccione ...',
        'url' => 'Direcciones/definiciones/parroquia'
    ],
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['estados_id']['options'] = Estados::pluck('nombre', 'id');
    }

    public function parroquia()
	{
		return $this->belongsTo('Modules\Direcciones\Model\Parroquia');
	}

	
}