var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.fnDraw();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"nombre","name":"nombre"},{"data":"slug","name":"slug"},{"data":"parroquia_id","name":"parroquia_id"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$("#nombre").blur(function(){
		$("#slug").val(slug($("#nombre").val()));
	});

	$('#estados_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'municipio_id','municipios');
	});

	$('#municipio_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'parroquia_id','parroquias');
	});
});