<?php

namespace Modules\Direcciones\Http\Controllers;

//Controlador Padre
use Modules\Direcciones\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Direcciones\Http\Requests\SectorRequest;

//Modelos
use Modules\Direcciones\Model\Sector;
use Modules\Direcciones\Model\Municipio;
use Modules\Direcciones\Model\Parroquia;


class SectorController extends Controller
{
    protected $titulo = 'Sector';

    public $js = [
        'Sector'
    ];
    
    public $css = [
        'Sector'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('direcciones::Sector', [
            'Sector' => new Sector()
        ]);
    }

    public function nuevo()
    {
        $Sector = new Sector();
        return $this->view('direcciones::Sector', [
            'layouts' => 'admin::layouts.popup',
            'Sector' => $Sector
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Sector = Sector::find($id);
        return $this->view('direcciones::Sector', [
            'layouts' => 'admin::layouts.popup',
            'Sector' => $Sector
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Sector = Sector::withTrashed()->find($id);
        } else {
            $Sector = Sector::find($id);
        }

        if ($Sector) {
            return array_merge($Sector->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(SectorRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Sector = $id == 0 ? new Sector() : Sector::find($id);

            $Sector->fill($request->all());
            $Sector->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Sector->id,
            'texto' => $Sector->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Sector::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Sector::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Sector::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Sector::select([
            'id', 'nombre', 'slug', 'parroquia_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function municipios(Request $request){
        $sql = Municipio::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'El estado no contiene Municipios'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Municipios encontradas', 'municipio_id'=> $sql];
        }               
        
        return $salida;
    }

    public function parroquias(Request $request){
        $sql = Parroquia::where('municipio_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'El Municipio no contiene Parroquias'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Parroquias encontradas', 'parroquia_id'=> $sql];
        }               
        
        return $salida;
    }
}