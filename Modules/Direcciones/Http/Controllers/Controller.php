<?php 

namespace Modules\Direcciones\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController {
	protected $titulo = 'Direcciones';
	public $prefijo_ruta = '';

	public $app = 'alimentos';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Direcciones/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Direcciones/Assets/css',
	];
}