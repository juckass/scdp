<?php
$menu['admin'][0]['menu'][] = [
	
	'nombre' 	=> 'Direcciones',
	'direccion' => '#Direcciones',
	'icono' 	=> 'fa fa-list',
	'menu' 		=>[
		[
			'nombre' 	=> 'Estados',
			'direccion' => 'direcciones/estados',
			'icono' 	=> 'fa fa-map'
		],	

		[
			'nombre' 	=> 'Ciudades',
			'direccion' => 'direcciones/ciudades',
			'icono' 	=> 'fa fa-map'
		],
		[
			'nombre' 	=> 'Municipio',
			'direccion' => 'direcciones/municipio',
			'icono' 	=> 'fa fa-map'
		],
		[ 
			'nombre' 	=> 'Parroquia',
			'direccion' => 'direcciones/parroquia',
			'icono' 	=> 'fa fa-map-marker'
		],
		[ 
			'nombre' 	=> 'Sector',
			'direccion' => 'direcciones/sector',
			'icono' 	=> 'fa fa-home'
		]
	]			

];

