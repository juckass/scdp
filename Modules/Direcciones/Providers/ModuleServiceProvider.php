<?php

namespace Modules\Direcciones\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'direcciones');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'direcciones');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'direcciones');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
