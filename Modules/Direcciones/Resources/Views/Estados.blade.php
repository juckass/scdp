@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Estados']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Estados.',
        'columnas' => [
            'Nombre' => '50',
		'Iso 3166-2' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Estados->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection