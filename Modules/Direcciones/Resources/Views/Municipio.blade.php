@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Municipio']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Municipio.',
        'columnas' => [
            'Estados' => '20',
		'Nombre' => '20',
		'Poblacion' => '20',
		'Opciones' => '20',
		'Path' => '20'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Municipio->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection