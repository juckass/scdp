@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Sector']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Sector.',
        'columnas' => [
            'Nombre' => '33.333333333333',
		'Slug' => '33.333333333333',
		'Parroquia' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Sector->generate(['estados_id', 'municipio_id', 'parroquia_id','nombre']) !!}
            <input type="hidden" name="slug" id="slug"/>
        {!! Form::close() !!}
    </div>
@endsection