<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'empresas'], function () {
    Route::group(['prefix' => 'definiciones/circuitos'], function() {
		Route::get('/', 				'CircuitosController@index');
		Route::get('nuevo', 			'CircuitosController@nuevo');
		Route::get('cambiar/{id}', 		'CircuitosController@cambiar');
		
		Route::get('buscar/{id}', 		'CircuitosController@buscar');

		Route::post('guardar',			'CircuitosController@guardar');
		Route::put('guardar/{id}', 		'CircuitosController@guardar');

		Route::delete('eliminar/{id}', 	'CircuitosController@eliminar');
		Route::post('restaurar/{id}', 	'CircuitosController@restaurar');
		Route::delete('destruir/{id}', 	'CircuitosController@destruir');

		Route::get('datatable', 		'CircuitosController@datatable');
	});

	Route::group(['prefix' => 'definiciones/entes'], function() {
		Route::get('/', 				'EnteController@index');
		Route::get('nuevo', 			'EnteController@nuevo');
		Route::get('cambiar/{id}', 		'EnteController@cambiar');
		
		Route::get('buscar/{id}', 		'EnteController@buscar');

		Route::post('guardar',			'EnteController@guardar');
		Route::put('guardar/{id}', 		'EnteController@guardar');

		Route::delete('eliminar/{id}', 	'EnteController@eliminar');
		Route::post('restaurar/{id}', 	'EnteController@restaurar');
		Route::delete('destruir/{id}', 	'EnteController@destruir');

		Route::get('datatable', 		'EnteController@datatable');
	});

	Route::group(['prefix' => 'definiciones/empresasdistribucion'], function() {
		Route::get('/', 				'EmpresasDistribucionController@index');
		Route::get('nuevo', 			'EmpresasDistribucionController@nuevo');
		Route::get('cambiar/{id}', 		'EmpresasDistribucionController@cambiar');
		
		Route::get('buscar/{id}', 		'EmpresasDistribucionController@buscar');

		Route::post('guardar',			'EmpresasDistribucionController@guardar');
		Route::put('guardar/{id}', 		'EmpresasDistribucionController@guardar');

		Route::delete('eliminar/{id}', 	'EmpresasDistribucionController@eliminar');
		Route::post('restaurar/{id}', 	'EmpresasDistribucionController@restaurar');
		Route::delete('destruir/{id}', 	'EmpresasDistribucionController@destruir');

		Route::get('datatable', 		'EmpresasDistribucionController@datatable');
	});

	Route::group(['prefix' => 'empresas'], function() {
		Route::get('/', 				'EmpresaController@index');
		Route::get('nuevo', 			'EmpresaController@nuevo');
		Route::get('cambiar/{id}', 		'EmpresaController@cambiar');
		
		Route::get('buscar/{id}', 		'EmpresaController@buscar');

		Route::post('guardar',			'EmpresaController@guardar');
		Route::put('guardar/{id}', 		'EmpresaController@guardar');

		Route::delete('eliminar/{id}', 	'EmpresaController@eliminar');
		Route::post('restaurar/{id}', 	'EmpresaController@restaurar');
		Route::delete('destruir/{id}', 	'EmpresaController@destruir');
		Route::get('ciudades/{id}',     'EmpresaController@ciudades');
		Route::get('municipios/{id}',   'EmpresaController@municipios');
		Route::get('parroquias/{id}',   'EmpresaController@parroquias');
	

		Route::get('datatable', 		'EmpresaController@datatable');
	});	

	Route::group(['prefix' => 'despachos'], function() {
		Route::get('/', 				'DespachosController@index');
		Route::get('nuevo', 			'DespachosController@nuevo');
		Route::get('cambiar/{id}', 		'DespachosController@cambiar');
		
		Route::post('buscar', 			'DespachosController@buscar');

		Route::post('guardar',			'DespachosController@guardar');
		Route::put('guardar/{id}', 		'DespachosController@guardar');

		Route::delete('eliminar/{id}', 	'DespachosController@eliminar');
		Route::post('restaurar/{id}', 	'DespachosController@restaurar');
		Route::delete('destruir/{id}', 	'DespachosController@destruir');
		Route::get('ciudades/{id}',     'DespachosController@ciudades');
		Route::get('municipios/{id}',   'DespachosController@municipios');
		Route::get('parroquias/{id}',   'DespachosController@parroquias');
	

		Route::get('datatable', 		'DespachosController@datatable');
	});

	Route::group(['prefix' => 'cargaempresa'], function() {
		Route::get('/',        'EmpresaCargaController@index');
		Route::post('/crear',  'EmpresaCargaController@crear');
	});

	Route::group(['prefix' => 'cargadespacho'], function() {
		Route::get('/',        'DespachoCargaController@index');
		Route::post('/crear',  'DespachoCargaController@crear');
	});
    
    Route::group(['prefix' => 'reporte'], function() {
		Route::get('/',  		  	  'ReporteController@index');
		Route::post('/imprimir/pdf',  'ReporteController@imprimir');
		Route::get('/imprimir',	  	  'ReporteController@imprimir');

	});

	Route::group(['prefix' => 'escritorio'], function() {
		Route::get('/',  				 					'EscritorioController@index');
		Route::post('/tablaemprasas',    					'EscritorioController@tablaemprasas');
		Route::post('/tablassaco',  	 					'EscritorioController@tablassaco');
		Route::post('/graficaemprasas',  					'EscritorioController@graficaemprasas');	
		Route::get('/municipios/{id}/{mes}/{ano}/{quien}',  'EscritorioController@municipios');
        Route::get('/datatable/{id}/{quien}', 	 			'EscritorioController@empreasas');	
        Route::get('/despachos/{id}/{quien}', 	 			'EscritorioController@despachos');	
        Route::post('/informacion', 	 					'EscritorioController@informacion');	
	});
});