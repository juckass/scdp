@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
	@include('admin::partials.botonera')
	
	@include('admin::partials.ubicacion', ['ubicacion' => ['Empresas Distribucion']])
	
	@include('admin::partials.modal-busqueda', [
		'columnas'=>[ 
			'Nombre' => '50',
			'Abastecimiento' => '50'
		]	
	])
@endsection

@section('content')
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
			{!! $EmpresasDistribucion->generate() !!}
		{!! Form::close() !!}
	</div>
@endsection