@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Ente']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Ente.',
        'columnas' => [
            'Nombre' => '50',
		'Slug' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Ente->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection