@extends(isset($layouts) ? $layouts : 'admin::layouts.default')'

@section('content-top')
	@include('admin::partials.botonera')
	
	@include('admin::partials.ubicacion', ['ubicacion' => ['Despachos']])
	
@endsection

@section('content')
	{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		
		
		<div class="row">
			{{ Form::bsSelect('mes', $controller->meses, '', [
				'label'      => 'Mes',
				'class_cont' => 'col-md-4 col-sm-6 col-xs-12',
				'required'   => 'required'
			]) }}

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
   				<label class="" for="mes">Año</label>
				<select name="ano" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="ano" required="required" class="form-control">
			

					@for ($i = 2017; $i <= date('Y') ; $i++)
						
						<option value="{{ $i }}" selected="selected">{{ $i }}</option>

					@endfor

				</select>
			</div>
		</div>

		<div class="row">
			<div class="portlet box green" id="empresas">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs"></i>Empresas</div>
					<div class="tools">
						<a href="javascript:;" class="collapse"> </a>
					
					</div>
				</div>
				<div class="portlet-body flip-scroll">
					<center>
						<table id="tabla" class="table table-striped table-hover table-bordered tables-text">
							<thead>
								<tr>
									<th style="width: 25%">Rif</th>
									<th style="width: 25%">Código Sica</th>
									<th style="width: 30%">Razón social</th>
									<th style="width: 20%">Despacho Mensual</th>
									
								</tr>
							</thead>
						</table>
					</center>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="portlet box green" id="Despachos">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs"></i>Despachos</div>
					<div class="tools">
						<a href="javascript:;" class="expand"> </a>
					</div>
				</div>
				<div class="portlet-body flip-scroll" style="display: none;">
					<table class="table table-striped table-hover table-bordered ">
						<thead>
							<tr>
								<th style="width: 25%">Rif</th>
								<th style="width: 25%">Codigo Sica</th>
								<th style="width: 30%">Razonsocial</th>
								<th style="width: 20%">Despacho Mensual</th>	
							</tr>
						</thead>
						<tbody>
							<tr>
								<td> <span id="rif"></span></td>
								<td> <span id="codigo_sica"></span></td>
								<td> <span id="razonsocial"></span></td>
								<td> <span id="despacho_mensual"></span></td>	
							</tr>
						</tbody>
					</table>
					
					<center>
						<table class="table table-striped table-hover table-bordered">
							<thead>
								<tr>
									<th>Entrega 1</th>
									<th>Entrega 2</th>
									<th>Entrega 3</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td  style="background-color: beige;">
										<input type="text"  class="btn col-md-12" name="fecha_primera" id="fecha_primera" placeholder="Fecha del Despacho"/>
										
									</td>
									<td  style="background-color: beige;">
										<input type="text"  class="btn col-md-12"  name="fecha_segunda" id="fecha_segunda" placeholder="Fecha del Despacho" />
										
									</td>
									<td  style="background-color: beige;">
										<input type="text"  class="btn col-md-12"  name="fecha_tersera" id="fecha_tersera" placeholder="Fecha del Despacho"/>
										
									</td>
								</tr>
								<tr>
									<td  style="background-color: beige;">
										
										<input type="number"  class="btn col-md-12" name="primera_entrega" id="primera_entrega"/>
									</td>
									<td  style="background-color: beige;">
										
										<input type="number"  class="btn col-md-12"  name="segunda_entrega" id="segunda_entrega"/>
									</td>
									<td  style="background-color: beige;">
									
										<input type="number"  class="btn col-md-12"  name="tercera_entrega" id="tercera_entrega"/>
									</td>
								</tr>
							</tbody>
						</table>
					</center>
					<h3>Comentarios</h3>
					<hr>
					<textarea name="comentarios" id="comentarios" style="margin: 0px; width: 1171px; height: 114px;"></textarea>
				</div>
			</div>
		</div>

    	<input type="hidden" name="empresa_id" id="empresa_id" value="">

	{!! Form::close() !!}
@endsection