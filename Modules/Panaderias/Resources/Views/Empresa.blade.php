@extends(isset($layouts) ? $layouts : 'admin::layouts.default')

@section('content-top')
    @include('admin::partials.botonera')
    
    @include('admin::partials.ubicacion', ['ubicacion' => ['Empresa']])
    
    @include('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Empresa.',
        'columnas' => [
            'Rif'         => '11.111111111111',
            'Codigo Sica' => '11.111111111111',
            'Razonsocial' => '11.111111111111',
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Empresa->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection