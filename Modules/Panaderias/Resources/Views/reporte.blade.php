@extends('admin::layouts.default')
@section('content')
	<div id="botonera">
		<!--
        <button id="buscar" class="btn green tooltips" data-container="body" data-placement="top" data-original-title="Inicio">
			<i class="fa fa-home"></i>
			<span class="visible-lg-inline visible-md-inline">Inicio</span>
		</button>
		<button id="atras" class="btn blue tooltips" data-container="body" data-placement="top" data-original-title="Atrás">
			<i class="fa fa-arrow-left"></i>
			<span class="visible-lg-inline visible-md-inline">Atr&aacute;s</span>
		</button>
        -->
		<button id="imprimir" class="btn btn-info tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.print.title') }}">
			<i class="fa fa-print"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.print.btn') }}</span>
		</button>
	</div>
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url(\Config::get('admin.prefix')) }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Reportes<i class="fa fa-circle"></i></span>
		</li>
		<li>
			<span>Reporte</span>
		</li>
	</ul>
    
	<div class="row">
		<div class="portlet box green" id="">
            <div class="portlet-title">
                <div class="caption">
                    Filtros
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll" style="overflow: auto;">
               	{!! Form::open([
					'id' => 'formulario',
					'name' => 'formulario',
					'method' => 'post',
					'target' => '_blank',
					'url' => 'empresas/reporte/imprimir/pdf'
				]) !!}
			
			    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
		            <label class="" for="mes">Año</label>
		            <select name="ano" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="ano" required="required" class="form-control">
		        

		                @for ($i = 2017; $i <= date('Y') ; $i++)
		                    
		                    <option value="{{ $i }}" selected="selected">{{ $i }}</option>

		                @endfor

		            </select>
		        </div>  

				{{ Form::bsSelect('mes', $controller->meses, date('m'), [
		            'label'      => 'Mes',
		            'required'   => 'required'
		        ]) }}

		        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
		            <label class="" for="despachos">Despachos</label>
		            <select name="despachos" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="despachos" required="required" class="form-control">
		                <option value="0" selected="selected">Todos</option>
		                <option value="1" >Despacho 1</option>
		                <option value="2" >Despacho 2</option>
		                <option value="3" >Despacho 3</option>
		            </select>
		        </div>
		        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
		            <label class="" for="despachos">Estatus</label>
		            <select name="falta" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="falta" required="required" class="form-control">
		                <option value="0" selected="selected">Despachados</option>
		                <option value="1" >Falta Por Despachar</option>       
		            </select>
		        </div>

		        {{ Form::bsSelect('municipio', $controller->municipio(),'', [
		            'label'      => 'Municipio',
		            'required'   => 'required'
		        ]) }}
					
				{!! Form::close() !!}
            </div>
        </div>

	</div>

	<div id="reportehtml"></div>
@endsection