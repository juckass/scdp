@extends(isset($layouts) ? $layouts : 'admin::layouts.popup2')
@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
         
        {!! Form::close() !!}

        <div class="row">
			<div class="portlet box green" id="empresas">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs"></i>Empresas</div>
					<div class="tools">
						<a href="javascript:;" class="collapse"> </a>
					
					</div>
				</div>
				<div class="portlet-body flip-scroll">
					<center>
						<table id="tabla" class="table table-striped table-hover table-bordered tables-text">
							<thead>
								<tr>
									<th style="width: 10%">Rif</th>
                                    <th style="width: 10%">Código Sica</th>
                                    <th style="width: 10%">Razón Social</th>
                                    <th style="width: 10%">Ente</th>
                                    <th style="width: 10%">Teléfonos</th>
								</tr>
							</thead>
						</table>
					</center>
				</div>
			</div>
		</div>
        <div class="row">
			<div class="portlet box green" id="infomacion">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-info-circle"></i>Infomación Detallada</div>
					<div class="tools">
						<a href="javascript:;" class="expand"> </a>
					</div>
				</div>
				<div class="portlet-body flip-scroll" style="display: none;">
                    <table class="table table-striped table-hover " id="info" border="1">
                        <tbody>
                            <tr>
                                <td width="50%" colspan="4"><center><b>Información de la Empresa<b></center></td>
                            </tr>
                            <tr>
                                <td width="20%"><b>Rif:</b> <br><span id="rif"></span></td>
                                <td width="20%"><b>Codigo Sica:</b><br> <span id="codigo_sica" </span></td>
                                <td width="60%" colspan="2"><b>Razón Social:</b><br> <span id="razonsocial"></span></td>
                            </tr>
                            <tr>
                                <td width="25%"><b>Circuito:</b> <br><span id="circuitos"></span></td>
                                <td width="25%"><b>Nivel:</b><br> <span id="nivel" </span></td>
                                <td width="25%" colspan="1"><b>Ente:</b><br> <span id="ente"></span></td>
                                <td width="25%"><b>Teléfonos:</b><br> <span id="telefonos"></span></td>
                            </tr>
                            <tr>
                                <td width="25%"><b>Estado:</b> <br><span id="estado"></span></td>
                                <td width="25%"><b>Municipio:</b><br> <span id="municipio" </span></td>
                                <td width="25%"><b>Parroquia:</b><br> <span id="parroquia"></span></td>
                                <td width="25%"><b>Ciudad:</b><br> <span id="ciudad"></span></td>
                            </tr>
                            <tr>
                                <td width="50%" colspan="4"><b>Dirección:</b> <br><span id="direccion"></span></td>
                            </tr>

                        </tbody>
                    </table>
			    </div>
		    </div>
        </div>
        <div class="row">
			<div class="portlet box green" id="despachos">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-info-circle"></i>Despachos</div>
					<div class="tools">
						<a href="javascript:;" class="expand"> </a>
					</div>
				</div>
				<div class="portlet-body flip-scroll" style="display: none;">
                    <center>
						<table id="tabla_despachos" class="table table-striped table-hover table-bordered tables-text">
							<thead>
								<tr>
									<th style="width: 12.5%">Primer despacho</th>
                                    <th style="width: 12.5%">Fecha primer despacho</th>
									<th style="width: 12.5%">Segundo despacho</th>
                                    <th style="width: 12.5%">Fecha segundo despacho</th>
									<th style="width: 12.5%">Tercer despacho</th>
                                    <th style="width: 12.5%">Fecha tercer despacho</th>
                                    <th style="width: 12.5%">Mes</th>
                                    <th style="width: 12.5%">Año</th>
								</tr>
							</thead>
						</table>
					</center>
			    </div>
		    </div>
        </div>
    </div>    
@endsection
@push('js')
<script>
    var mes = {{ $mes }}; 
    var ano = {{$ano}};
    var id  = {{$id}};
    var quien ='{{$quien}}';
</script>
@endpush
@push('js')
<style>
#info{
    box-shadow: 1px 0px 6px black;
}
</style>
@endpush