<!DOCTYPE html>
<html>
<head>
	<title>Reporte del clap {{ $clap->codigo }}</title>
	<style type="text/css">
		table {
			font-family: Arial !important;
			font-size: 12px;
		}
		#cuerpo{
    		font-family: Arial !important;
			
			}
		#cuerpo table{
	    	font-family: Arial !important;
	    	margin: auto;
	    	width: 100%;
	 		border: 1px solid;
	 		border-collapse: collapse;
	    }
	    #cuerpo table .titulo{
	    	width: 40%;
	    }
	    #cuerpo table .titulo_calle{
	    	text-align: center
	       
	    }
	    #cuerpo table .titulo_tabla{
	    	text-align: center;
	    	background: #D3D0D0;
	    }
	    #firmas p{
	    	font-size: 12px;
	    	text-align: justify;
	    	
	    }
	    #firmas table{
	    	font-size: 12px;
	    	width: 100%;
	    	border-collapse: collapse;
	    }
	</style>
    
</head>
<body>
	<div id="cuerpo">
		<br>
		<br/>
		<h4 id="titulo"  align="center">CERTIFICACION DE REGISTRO DE COMITÉ LOCAL DE ABASTECIMIENTO Y PRODUCCIÓN - CLAP</h4>
		<!-- tabla de datos de la comunidad-->
		<table border="1">
			<tr>
				<td class="titulo_tabla" colspan="2">
					<b>DATOS DE LA COMUNIDAD</b>
				</td>
				
			</tr>
			<tr>
				<td class="titulo">
					<b>Estado</b>
				</td>
				<td>{{ strtoupper($ubicacion->estado) }}</td>
			</tr>
			<tr>
				<td>
					<b>Municipio</b>
				</td>
				<td>{{ strtoupper($ubicacion->municipio) }}</td>
			</tr>
			<tr>
				<td>
					<b>Parroquia</b>
				</td>
				<td>{{ strtoupper($ubicacion->parroquia) }}</td>
			</tr>
			<tr>
				<td>
					<b>Sector</b>
				</td>
				<td>{{ strtoupper($ubicacion->sector) }}</td>
			</tr>
			<tr>
				<td>
					<b>Consejo Comunal</b>
				</td>
				<td>{{ strtoupper($clap->consejo_comunal) }}</td>
			</tr>
			<tr>
				<td>
					<b>Nombre de la comunidad</b>
				</td>
				<td>{{ strtoupper($clap->nombre) }}</td>
			</tr>
		</table>
		<br>
		<!-- tabla de datos del clap-->

		<table border="1">
			<tr>
				<td class="titulo_tabla" colspan="2">
					<b>DATOS DEL CLAP</b>
				</td>
				
			</tr>
			<tr>
				<td class="titulo">
					<b>Nombre del CLAP</b>
				</td>
				<td>{{ $clap->nombre }}</td>
			</tr>
			<tr>
				<td>
					<b>Código del CLAP</b>
				</td>
				<td>{{ $clap->codigo }}</td>
			</tr>
			<tr>
				<td>
					<b>Líder de Comunidad</b>
				</td>
				<td>{{ strtoupper($responsable['lider']) }}</td>
			</tr>
			<tr>
				<td>
					<b>Responsable UBCH</b>
				</td>
				<td>{{ strtoupper($responsable['ubch']) }}</td>
			</tr>
			<tr>
				<td>
					<b>Responsable UnaMujer</b>
				</td>
				<td>{{ strtoupper($responsable['unaMujer']) }}</td>
			</tr>
			<tr>
				<td>
					<b>Responsable FFM/Juventud</b>
				</td>
				<td>{{ strtoupper($responsable['ffm']) }}</td>
			</tr>
			<tr>
				<td>
					<b>Responsable Consejo Comunal</b>
				</td>
				<td>{{ strtoupper($responsable['cc']) }}</td>
			</tr>
		</table>
		<!-- tabla de datos de los responsables de calle-->
		<br>
		<table border="1">
			<tr>
				<td class="titulo_tabla" colspan="5">
					<b>RESPONSABLES DE CALLE</b>
				</td>
				
			</tr>
			<tr>
				<td class="titulo_calle">
					<b>Nombre de la Calle</b>
				</td>
				<td class="titulo_calle">
					<b>Nombre del Líder de Calle</b>
				</td>
				<td class="titulo_calle">
					<b>CI Líder de Calle</b>
				</td>
				<td class="titulo_calle">
					<b>Familias</b>
				</td>
				<td class="titulo_calle">
					<b>Cargadas</b>
				</td>
				
			</tr>
			@foreach($responsable_calle as $res)
				<tr>
					<td style="padding: 5px;">{{ strtoupper($res->nombre_calle) }}</td>
					<td style="padding: 5px;">{{ strtoupper($res->nombre) }}</td>
					<td style="padding: 5px;">{{ strtoupper($res->cedula) }}</td>
					<td style="padding: 5px;">{{ strtoupper($res->familias) }}</td>
					<td style="padding: 5px;">0</td>
				</tr>
			@endforeach
		</table>
		<br>

	</div>
	<div id="firmas">
		<p>
			La presente certificación de registro de CLAP, se expide por el Sistema de Registro de CLAP para el Sistema Popular de Distribución de Alimentos del municipio: <b>{{ strtoupper($ubicacion->municipio) }}</b>, del estado <b>{{ strtoupper($ubicacion->estado) }}</b>; a los <b>{{ $fecha['d'] }}</b> dias del mes de <b>{{ $fecha['m'] }}</b> del año <b>{{ $fecha['Y'] }}</b>; a los fines de dejar constancia que dicha comunidad cumplió con los requisitos necesarios a fin.
		</p>
		<table >
			<tr>
				<td>
					CERTIFICADO: <br>
					JEFE DE LA SALA DE REGISTRO
				</td>
				<td>
					CERTIFICADO: <br>
					JEFE DE PUESTO DE COMANDO ESTADAL
				</td>

			</tr>
		</table>
		
	</div>


</body>
</html>