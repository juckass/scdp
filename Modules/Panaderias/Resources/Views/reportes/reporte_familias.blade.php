<!DOCTYPE html>
<html>
<head>
	<title>Reporte del clap {{ $clap->codigo }}</title>
	<style type="text/css">
		table {
			font-family: Arial !important;
			font-size: 12px;
		}
		#cargaFamiliar table tr{
			align-content: center;
		}
		#cuerpo{
    		font-family: Arial !important;
			
			}
		#cuerpo table{
	    	font-family: Arial !important;
	    	margin: auto;
	    	width: 100%;
	 		border: 1px solid;
	 		border-collapse: collapse;
	    }
	    #cuerpo table .titulo{
	    	width: 40%;
	    }
	    #cuerpo table .titulo_calle{
	    	text-align: center
	       
	    }
	    #cuerpo table .titulo_tabla{
	    	text-align: center;
	    	background: #D3D0D0;
	    }
	    #firmas p{
	    	font-size: 12px;
	    	text-align: justify;
	    	
	    }
	    #firmas table{
	    	font-size: 12px;
	    	width: 100%;
	    	border-collapse: collapse;
	    }
	</style>
    
</head>
<body>
	<div id="cuerpo">

		<h4 id="titulo"  align="center">REGISTRO DE CARGA FAMILIAR - CLAP</h4>
		<!-- tabla de datos de la comunidad-->
		<table border="1">
			<tr>
				<td class="titulo_tabla" colspan="9">
					<b>DATOS DEL CLAPS</b>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<b>Estado:</b> {{ strtoupper($clap->estado) }}
				</td>
				<td colspan="3">
					<b>Municipio:</b> {{ strtoupper($clap->municipio) }}
				</td>
				<td colspan="3">
					<b>Parroquia:</b> {{ strtoupper($clap->parroquia) }}
				</td>

			</tr>
			<tr width="25%">
				<td colspan="2">
					<b>CLAPS:</b> {{ $clap->nombre }}
				</td>	
				
				<td colspan="4"> 
					<b>Comunidad: </b> {{ strtoupper($clap->consejo_comunal) }}
				</td>
				
				<td colspan="3">
					<b>Lider de Clap:</b> {{ strtoupper($clap->lider_clap) }}
				</td>
				
			</tr>
			<tr>
				<td colspan="2">
					<b>Sector:</b> {{ strtoupper($clap->sector) }}
				</td>	
				
				<td colspan="7">
					<b>Jefe de Familia:</b> {{ strtoupper( $jefe_familia->nombre ) }}
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>Lider de Calle:</b> {{strtoupper( $lider_calle->lider_calle ) }}
				</td>	
				
				<td colspan="5">
					<b>Dirección:</b> {{ strtoupper( $jefe_familia->direccion) }}
				</td>
				
				<td colspan="2">
					<b>Casa Nro:</b> 
					@if($jefe_familia->nro_casa != '')
						{{  strtoupper($jefe_familia->nro_casa) }}
					@else
						S/N
					@endif
				</td>
			</tr>
		</table>
		<br>
		<table border="1" id="cargaFamiliar">
			<tr>
				<td class="titulo_tabla" colspan="10">
					<b>INTEGRATES DE LA FAMILIA</b>
				</td>
			</tr>
			<tr>
				<td class="titulo_calle">
					<b>Nro</b>
				</td>
				<td class="titulo_calle">
					<b>Nombre y Apellido</b>
				</td>
				<td class="titulo_calle">
					<b>Cédula</b>
				</td>
				<td class="titulo_calle">
					<b>PSUV</b>
				</td>
				<td class="titulo_calle">
					<b>Correo</b>
				</td>
				<td class="titulo_calle">
					<b>Telefono</b>
				</td>
				<td class="titulo_calle">
					<b>PSUV</b>
				</td>
				<td class="titulo_calle">
					<b>Parentesco</b>
				</td>
				<td class="titulo_calle">
					<b>Genero</b>
				</td>
				<td class="titulo_calle">
					<b>Condicion Especial</b>
				</td>
				
			</tr>
			<?php $n = 1; ?>
			@foreach($integrantes as $i)
				<tr style="text-align: center;">
					<td style="padding: 5px;"> <b>{{ $n }} </b> </td>
					<td style="padding: 5px;">{{ strtoupper($i->nombre) }}</td>
					<td style="padding: 5px;">{{ strtoupper($i->cedula) }}</td>
					<td style="padding: 5px;"> N/A </td>
					<td style="padding: 5px;">{{ strtoupper($i->correo) }}</td>
					<td style="padding: 5px;">{{ strtoupper($i->telefono) }}</td>
					<td style="padding: 5px;"> 
						@if($i->partida_nac == '')
							N/A
						@else
							{{ strtoupper($i->partida_nac) }}
						@endif

					</td>
					<td style="padding: 5px;">{{ strtoupper($i->parentesco) }}</td>
					<td style="padding: 5px;">{{ strtoupper($i->genero) }}</td>
					<td style="padding: 5px;"> 
						@if($i->condicion_especial == '')
							N/A
						@else
							{{ strtoupper($i->condicion_especial) }}
						@endif

					</td>
				</tr>

				<?php $n++;?>
			@endforeach
			@if($n < 5)
				@for($e = 0; $e < 6; $e++)
					<tr>
						<td style="padding: 5px;"> <b>{{ $n }} </b> </td>
						<td style="padding: 5px;"></td>
						<td style="padding: 5px;"></td>
						<td style="padding: 5px;"></td>
						<td style="padding: 5px;"></td>
						<td style="padding: 5px;"></td>
						<td style="padding: 5px;"></td>
						<td style="padding: 5px;"></td>
						<td style="padding: 5px;"></td>
						<td style="padding: 5px;"></td>
					<?php $n++ ?>
				@endfor	</tr>
			@endif
		</table>
		<br>

		<table border="1">
			<tr >
				<td style="height: 50px">
					<b>Obervaciones:</b>
				</td>
			</tr>
		</table>

	</div>


</body>
</html>