@extends(isset($layouts) ? $layouts : 'admin::layouts.default')'

@section('content-top')
	
	@include('admin::partials.ubicacion', ['ubicacion' => ['Despachos']])
	
@endsection

@section('content')
    <div id="botonera">
		<div class="btn-group btn-group-solid">
			<button id="guardar2" class="btn blue tooltips" data-original-title="Guardar Registro" data-placement="top" data-container="body">Guardar</button>
		</div>
	</div>
	{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		
		
		<div class="row">
			{{ Form::bsSelect('mes', $controller->meses, '', [
				'label'      => 'Mes',
				'class_cont' => 'col-md-4 col-sm-6 col-xs-12',
				'required'   => 'required'
			]) }}

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
   				<label class="" for="mes">Año</label>
				<select name="ano" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="ano" required="required" class="form-control">
			

					@for ($i = 2017; $i <= date('Y') ; $i++)
						
						<option value="{{ $i }}" selected="selected">{{ $i }}</option>

					@endfor

				</select>
			</div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Cargar Archivo <small>Excel (.xls, .xlsx, .ods)</small></h4>
                <hr>
                <input id="upload" name="subir" type="file"/>
                <button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
                    <span class="ladda-label">
                        <i class="icon-arrow-right"></i> Carga archivo de Despachos
                    </span>
                </button>
	        </div>
		</div>


	{!! Form::close() !!}
@endsection