@extends(isset($layouts) ? $layouts : 'admin::layouts.default')'

@section('content-top')
	
	@include('admin::partials.ubicacion', ['ubicacion' => ['Despachos']])
	
@endsection

@section('content')
    <div id="botonera">
		<div class="btn-group btn-group-solid">
			<button id="guardar2" class="btn blue tooltips" data-original-title="Guardar Registro" data-placement="top" data-container="body">Guardar</button>
		</div>
	</div>
	{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		<div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Cargar Archivo <small>Excel (.xls, .xlsx)</small></h4>
                <hr>
                <input id="upload" name="subir" type="file"/>
                <button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
                    <span class="ladda-label">
                        <i class="icon-arrow-right"></i> Carga archivo de empresas
                    </span>
                </button>
	        </div>
		</div>
	{!! Form::close() !!}
@endsection