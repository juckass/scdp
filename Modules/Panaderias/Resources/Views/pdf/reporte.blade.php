@if (!$html)
@include('panaderias::pdf.estilo_reporte')
@endif
<div id="reporteHtml" style="">
	<div class="header clearfix">
		<div id="logo" style="">	
			<div class="col-md-4 logo" id="img1">
				<img src="{{ url('public/img/logos/logo2.png') }}"  style="width: 4cm;margin-bottom: 15px;">
				<br>
			</div>
			<div class="col-md-4">
			
			</div>

			<div class="col-md-4 logo" id="img2">
				<img src="{{ url('public/img/logos/sunagro.png') }}" style="width: 4cm;" >
			</div>
		</div>
		
		<div>
			<h1>REPORTE DE DESPACHOS DE PANADERIAS</h1>
		</div>
		
		<div id="company" class="clearfix">
			<div style="margin-right: 35px;"><span>Fecha de impresi&oacute;n</span> {{ date('d/m/Y') }}</div>
		</div>
	</header>
	<div class="main">  
		<table id="tabla">
			<thead>
				<tr>
					<th>Rif</th>

					<th>Código Sica</th>

					<th>Razón social</th>

					<th>Ente</th>

					<th>Despacho Men.</th>

					<th>Desp. 1</th>
					<th>Fecha</th>

					<th>Desp. 2</th>
					<th>Fecha</th>

					<th>Desp. 3</th>
					<th>Fecha</th>

					<th>Total</th>

					<th class="comentario">Comentarios</th>
					
				</tr>
			</thead>
			<tbody>

				@foreach($datos as $reporte)
					<tr data-empresa-id="{{  $reporte->empresaid }}">
						<td class="">{{ $reporte->rif }}</td>
						<td class="">{{ $reporte->codigo_sica }}</td>
						<td class="">{{ $reporte->razonsocial }}</td>
						<td class="">{{ $reporte->ente }}</td>
						<td class="">{{ $reporte->despacho_mensual }}</td>

						<td class="">{{ $reporte->primera_entrega }}</td>

						<td class="">{{ $reporte->fecha_primera }}</td>

						<td class="">{{ $reporte->segunda_entrega }}</td>

						<td class="">{{ $reporte->fecha_segunda }}</td>

						<td class="">{{ $reporte->tercera_entrega }}</td>

						<td class="">{{ $reporte->fecha_tersera }}</td>

						<td class="">{{ $reporte->primera_entrega +  $reporte->segunda_entrega +  $reporte->fecha_tersera  }}</td>
						
						@if($reporte->comentarios == '')
							<td class="comentario"></td>
						@else
							<td class="comentario"><i class="fa fa-comment-o" aria-hidden="true"></i></td>
						@endif
					</tr>

				@endforeach
				<!-- <tr style="font-weight: bold;">
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>					
				</tr> -->
			</tbody>
		</table>	
	</div>
	
</div>