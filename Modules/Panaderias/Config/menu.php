<?php

$menu['panaderias'] = [
	[
		'nombre' 	=> 'Inicio',
		'direccion' => 'empresas/escritorio',
		'icono' 	=> 'fa fa-home',
	],
	[
		'nombre' 	=> 'Empresas',
		'direccion' => '#Empresas',
		'icono' 	=> 'fa fa-industry',
		'menu' 		=> [			
			[
				'nombre' 	=> 'Definiciones',
				'direccion' => '#Definicionesclaps',
				'icono' 	=> 'fa fa-hashtag',
				'menu' 		=>[
					[
						'nombre' 	=> 'Circuitos',
						'direccion' => 'empresas/definiciones/circuitos',
						'icono' 	=> 'fa fa-bullseye'
					],[
						'nombre' 	=> 'Entes',
						'direccion' => 'empresas/definiciones/entes',
						'icono' 	=> 'fa fa-university'
					],
					[
						'nombre' 	=> 'Empresas De Distribucion',
						'direccion' => 'empresas/definiciones/empresasdistribucion',
						'icono' 	=> 'fa fa-industry'
					],
				]			
			],
			[
				'nombre' 	=> 'Empresa',
				'direccion' => 'empresas/empresas',
				'icono' 	=> 'fa fa-industry',
			],
			[ 
				'nombre' 	=> 'Carga masiva',
				'direccion' => '#carga',
				'icono' 	=> 'fa fa-upload',
				'menu' 		=> [
					[
						'nombre' 	=> 'Carga Empresas',
						'direccion' => 'empresas/cargaempresa',
						'icono' 	=> 'fa fa-upload',
					],
					[
						'nombre' 	=> 'Carga Despachos',
						'direccion' => 'empresas/cargadespacho',
						'icono' 	=> 'fa fa-upload',
					]
				]
			]		    
		]
	],
	[
		'nombre' 	=> 'Despachos',
		'direccion' => 'empresas/despachos',
		'icono' 	=> 'fa fa-truck',
	],
	[
		'nombre' 	=> 'Reportes',
		'direccion' => '#Reportes',
		'icono' 	=> 'fa fa-print',
		'menu' 		=> [
			[
				'nombre' 	=> 'Reporte',
				'direccion' => 'empresas/reporte',
				'icono' 	=> 'fa fa-print',
			]   
		]
	]
];