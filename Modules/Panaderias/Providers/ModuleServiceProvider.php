<?php

namespace Modules\Panaderias\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'panaderias');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'panaderias');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'panaderias');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
