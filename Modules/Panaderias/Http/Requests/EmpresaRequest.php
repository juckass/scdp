<?php

namespace Modules\Panaderias\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request {
    protected $reglasArr = [
		'circuitos_id' => ['required', 'integer'], 
		'rif' => ['required', 'min:3', 'max:255', 'unique:empresa,rif'], 
		'codigo_sica' => ['required', 'integer'], 
		'razonsocial' => ['required', 'min:3', 'max:255'], 
		'nivel' => ['required', 'integer'], 
		'ente_id' => ['required', 'integer'], 
		'fecha_solicitud' => ['required', 'date_format:"d/m/Y"'], 
		'estados_id' => ['integer']
	];
}