<?php

namespace Modules\Panaderias\Http\Requests;

use App\Http\Requests\Request;
 
class DespachosRequest extends Request {
	protected $reglasArr = [
		'empresa_id' => ['required', 'integer'], 
		'mes' => ['required', 'integer'], 
		'ano' => ['required', 'integer'], 
		'primera_entrega' => ['integer'], 
		'segunda_entrega' => ['integer'], 
		'tercera_entrega' => ['integer'], 
		'comentarios' => ['']
	];
}