<?php

namespace Modules\Panaderias\Http\Requests;

use App\Http\Requests\Request;

class EmpresaCargaRequest extends Request {
    protected $reglasArr = [
		'subir' => ['required']
	];
}