<?php

namespace Modules\Panaderias\Http\Requests;

use App\Http\Requests\Request;
 
class EmpresasDistribucionRequest extends Request {
	protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:255'], 
		'abastecimiento' => ['required', 'integer']
	];
}