<?php

namespace Modules\Panaderias\Http\Controllers;

//Controlador Padre
use Modules\Panaderias\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Excel; 
//Request
use Modules\Panaderias\Http\Requests\CircuitosRequest;
use Carbon\Carbon;
//Modelos
use Modules\Panaderias\Model\Circuitos;
use Modules\Panaderias\Model\Ente;

use Modules\Panaderias\Model\Empresa;
use Modules\Direcciones\Model\Estados;
use Modules\Direcciones\Model\Municipio;
use Modules\Direcciones\Model\Parroquia;
use Modules\Direcciones\Model\Ciudades;
use Modules\Panaderias\Model\Despachos;

class CargaController extends Controller
{
    protected $titulo = 'Carga';
    
    public $meses = [		
		1 =>'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];

    public $js = [
        'Carga'
    ];
    
    public $css = [
        'Carga'
    ];

    public $librerias = [
        'ladda',
    ];

    public function index()
    {
        
        return $this->view('panaderias::Carga');
    }
    public function crear(Request $request)
    {
        set_time_limit(0);
		DB::beginTransaction();
		try {
			$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			];

			$ruta = public_path('archivos/');
			$archivo = $request->file('subir');

			$mime = $archivo->getClientMimeType();
			
			do {
				$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
			} while (is_file($ruta . $nombre_archivo));
				
			$archivo->move($ruta, $nombre_archivo);

			chmod($ruta . $nombre_archivo, 0777);

			$excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();
           
            $estados = [];
            foreach(Estados::all() as $estado) {
                $estados[str_slug($estado->nombre, '-')] = $estado->id;
            }
            $ciudades = [];
            foreach(Ciudades::all() as $ciudad) {
                $ciudades[str_slug($ciudad->nombre, '-')] = $ciudad->id;
            }

            $municipios = [];
            foreach(Municipio::all() as $municipio) {
                $municipios[str_slug($municipio->nombre, '-')] = $municipio->id;
            }
                

            $parroquias = [];
            foreach(parroquia::all() as $parroquia) {
                $parroquias[str_slug($parroquia->nombre, '-')] = $parroquia->id;
            }

           /* if (isset($estados[str_slug($empresa->estado)])) {
                $empresa->estado = $estados[str_slug($empresa->estado)];
            } else {
                $empresa->estado = null;
            }*/

            $estatus = [];
            $estatus['activa'] = 0;
            $estatus['bloqueada'] = 1;
            $estatus['cerrada'] = 2;
            $estatus['inactiva'] = 3;

               /*  0 => 'Activa',
                1 => 'Bloqueada',
                2 => 'Cerrada',
                3 => 'Inactivo' */
                
	 		foreach ($excel as $key => $value) {
	 			
                 /*if($value->rif == '' || $value->codigo_sica == ''){
                    continue;
                 }*/

                if($value->cuota_mensual !=''){
                    $despacho_mensual = intval($value->cuota_mensual);
                }else{
                     $despacho_mensual = 0;
                }
                
	 			 $_circuito = null;

                 if($value->circuito != ''){
                    $circuitos = Circuitos::updateOrCreate(
                        [
                            "slug" => str_slug($value->circuito, '-'),
                        ],
                        [
                            "nombre" => $value->circuito
                        ]
				    );
                     $_circuito =  $circuitos->id;
                 }

                $_ente = null;
                if($value->ente != '' ){
                    $ente =	Ente::updateOrCreate(
                        [
                            "slug" => str_slug($value->ente, '-'),
                        ],
                        [
                            "nombre" => $value->ente
                        ]
				    );
                    
                    $_ente = $ente->id;
                }
	 			
	 			
                if (isset($estados[str_slug($value->estado)])) {
                    $_estado = $estados[str_slug($value->estado)];
                } else {
                    $_estado = null;
                }

                if (isset($municipios[str_slug($value->municipio)])) {
                    $_municipio = $municipios[str_slug($value->municipio)];
                } else {
                    $_municipio = null;
                }
                if (isset($parroquias[str_slug($value->parroquia)])) {
                    $_parroquia = $parroquias[str_slug($value->parroquia)];
                } else {
                    $_parroquia = null;
                }
                if (isset($ciudades[str_slug($value->ciudad)])) {
                    $_ciudad = $ciudades[str_slug($value->ciudad)];
                } else {
                    $_ciudad = null;
                }

                
                if(str_slug($value->municipio) == 'raul-leoni-angostura'){
                    $_municipio = 72;
                }
                 $_estatus= 0;
                if($value->estatus !=''){
                    $_estatus = $estatus[strtolower($value->estatus)] ;
                }

               $empresa = Empresa::Create(
					[
					    "rif"              => $value->rif,
                        "codigo_sica"      => $value->codigo_sica,
						"circuitos_id"     => $_circuito,
                        "razonsocial"      => $value->razon_social,
                        "nivel"            => $value->nivel,
                        "ente_id"          => $_ente,
                        "estados_id"       => $_estado,
                        "ciudades_id"      => $_ciudad,
                        "municipio_id"     => $_municipio,
                        "parroquia_id"     => $_parroquia,
                        "direccion"        => $value->direccion,
                        "telefonos"        => $value->telefono,
                        "despacho_mensual" => $despacho_mensual,
                        "estado_empresa"   => $_estatus,
                        "observacion"      => $value->observaciones
					]
                  
				);  

                $uno  = is_numeric($value->despacho_1_3) ? $value->despacho_1_3 : 0;
                $dos  = is_numeric($value->despacho_2_3) ? $value->despacho_2_3 : 0;
                $tres = is_numeric($value->despacho_3_3) ? $value->despacho_3_3 : 0;
            
               $resultado = $uno + $dos + $tres;

               if($resultado != 0 ){
                    $Despachos = Despachos::updateOrCreate(
                        [
                            "empresa_id" =>  $empresa->id,
                            "mes"=> $request->mes,
                            "ano"=> $request->ano
                        ],
                        [
                            "primera_entrega" => $uno,
                            "segunda_entrega" => $dos,
                            "tercera_entrega" => $tres,
                            "comentarios"	  => '',
                            'fecha_primera'   => $value->fecha_de_despacho_1_3,
                            'fecha_segunda'   => $value->fecha_de_despacho_2_3,
                            'fecha_tersera'   => $value->fecha_de_despacho_3_3

                        ]
				    );

                } 
                
	 		}
		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir'),];
    }
}