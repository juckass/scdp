<?php

namespace Modules\Panaderias\Http\Controllers;

//Controlador Padre
use Modules\Panaderias\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Panaderias\Http\Requests\CircuitosRequest;

//Modelos
use Modules\Panaderias\Model\Circuitos;

class CircuitosController extends Controller
{
    protected $titulo = 'Circuitos';

    public $js = [
        'Circuitos'
    ];
    
    public $css = [
        'Circuitos'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('panaderias::Circuitos', [
            'Circuitos' => new Circuitos()
        ]);
    }

    public function nuevo()
    {
        $Circuitos = new Circuitos();
        return $this->view('panaderias::Circuitos', [
            'layouts' => 'admin::layouts.popup',
            'Circuitos' => $Circuitos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Circuitos = Circuitos::find($id);
        return $this->view('panaderias::Circuitos', [
            'layouts' => 'admin::layouts.popup',
            'Circuitos' => $Circuitos
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Circuitos = Circuitos::withTrashed()->find($id);
        } else {
            $Circuitos = Circuitos::find($id);
        }

        if ($Circuitos) {
            return array_merge($Circuitos->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(CircuitosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Circuitos = $id == 0 ? new Circuitos() : Circuitos::find($id);

           
            $data = $request->all();
            $data['slug'] = str_slug($request->nombre, '-');

            $Circuitos->fill($data);
            $Circuitos->save();


        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Circuitos->id,
            'texto' => $Circuitos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Circuitos::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Circuitos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Circuitos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Circuitos::select([
            'id', 'nombre', 'slug', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}