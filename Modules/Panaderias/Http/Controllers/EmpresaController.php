<?php

namespace Modules\Panaderias\Http\Controllers;

//Controlador Padre
use Modules\Panaderias\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Panaderias\Http\Requests\EmpresaRequest;

//Modelos
use Modules\Panaderias\Model\Empresa;
use Modules\Direcciones\Model\Estados;
use Modules\Direcciones\Model\Municipio;
use Modules\Direcciones\Model\Parroquia;
use Modules\Direcciones\Model\Ciudades;


class EmpresaController extends Controller
{
    protected $titulo = 'Empresa';

    public $js = [
        'Empresa'
    ];
    
    public $css = [
        'Empresa'
    ];

    public $librerias = [
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker'
    ];

    public function index()
    {
        return $this->view('panaderias::Empresa', [
            'Empresa' => new Empresa()
        ]);
    }

    public function nuevo()
    {
        $Empresa = new Empresa();
        return $this->view('panaderias::Empresa', [
            'layouts' => 'admin::layouts.popup',
            'Empresa' => $Empresa
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Empresa = Empresa::find($id);
        return $this->view('panaderias::Empresa', [
            'layouts' => 'admin::layouts.popup',
            'Empresa' => $Empresa
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Empresa = Empresa::withTrashed()->find($id);
        } else {
            $Empresa = Empresa::find($id);

        }
            
        
        if ($Empresa) {
           
            /*$municipio = Parroquia::select('municipio_id')
                ->where('id',  $Empresa->parroquia_id)
                ->first()
                ->municipio_id;
            
            $estado = Municipio::select('estados_id')
                ->where('id', $municipio)
                ->first()
                ->estados_id;
            */

            $estados_id = Estados::pluck('nombre','id')
                ->put('_', $Empresa->estados_id);

            $ciudades_id ='';
            if($Empresa->ciudades_id != ''){
                 $ciudades_id = ciudades::where('estados_id',  $Empresa->estados_id )
                ->pluck('nombre','id')
                ->put('_', $Empresa->ciudades_id);
            }
            
            $municipio_id = '';
            if($Empresa->municipio_id !=''){
                $municipio_id = Municipio::pluck('nombre','id')
                ->put('_', $Empresa->municipio_id);
            }
             $parroquia_id = '';
            if($Empresa->parroquia_id != ''){
                $parroquia_id = Parroquia::where('id', $Empresa->parroquia_id)
                ->pluck('nombre','id')
                ->put('_', $Empresa->parroquia_id);
            }


            return array_merge($Empresa->toArray(), [
                'estados_id'   => $estados_id,
                'municipio_id' => $municipio_id,
                'parroquia_id' => $parroquia_id,
                'ciudades_id' => $ciudades_id,
                's'            => 's',
                'msj'          => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EmpresaRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Empresa = $id == 0 ? new Empresa() : Empresa::find($id);

            $Empresa->fill($request->all());
            $Empresa->save();
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Empresa->id,
            'texto' => $Empresa->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Empresa::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Empresa::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Empresa::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Empresa::select([
            'id', 'circuitos_id', 'rif', 'codigo_sica', 'razonsocial', 'nivel', 'ente_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function ciudades(Request $request){
        $sql = Ciudades::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el estado no Contiene ciudades'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Ciudades encontrados', 'ciudades_id'=> $sql];
        }               
        
        return $salida;
    } 
    
    public function municipios(Request $request){
        $sql = Municipio::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el estado no Contiene municipios'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Municipios encontrados', 'municipio_id'=> $sql];
        }               
        
        return $salida;
    } 
    public function parroquias(Request $request){
        $sql = Parroquia::where('municipio_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el municipio no Contiene parroquias'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Paroquias encontrados', 'parroquia_id'=> $sql];
        }               
        
        return $salida;
    } 
    public function sectores(Request $request){
        $sql = Sector::where('parroquias_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'La parroquia no Contiene sectores'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Sectores encontrados', 'sectores_id'=> $sql];
        }               
        
        return $salida;
    }
}