<?php

namespace Modules\Panaderias\Http\Controllers;

//Controlador Padre
use Modules\Panaderias\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Panaderias\Http\Requests\EnteRequest;

//Modelos
use Modules\Panaderias\Model\Ente;

class EnteController extends Controller
{
    protected $titulo = 'Ente';

    public $js = [
        'Ente'
    ];
    
    public $css = [
        'Ente'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('panaderias::Ente', [
            'Ente' => new Ente()
        ]);
    }

    public function nuevo()
    {
        $Ente = new Ente();
        return $this->view('panaderias::Ente', [
            'layouts' => 'admin::layouts.popup',
            'Ente' => $Ente
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Ente = Ente::find($id);
        return $this->view('panaderias::Ente', [
            'layouts' => 'admin::layouts.popup',
            'Ente' => $Ente
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Ente = Ente::withTrashed()->find($id);
        } else {
            $Ente = Ente::find($id);
        }

        if ($Ente) {
            return array_merge($Ente->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EnteRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Ente = $id == 0 ? new Ente() : Ente::find($id);

            $data = $request->all();
            $data['slug'] = str_slug($request->nombre, '-');

            $Ente->fill($data);
            $Ente->save();
            
        } catch(QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        } catch(Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }
        DB::commit();

        return [
            'id'    => $Ente->id,
            'texto' => $Ente->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Ente::destroy($id);
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Ente::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Ente::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Ente::select([
            'id', 'nombre', 'slug', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}