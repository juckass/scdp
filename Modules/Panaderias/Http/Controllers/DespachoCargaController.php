<?php

namespace Modules\Panaderias\Http\Controllers;

//Controlador Padre
use Modules\Panaderias\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Excel; 
//Request
use Modules\Panaderias\Http\Requests\CircuitosRequest;
use Carbon\Carbon;
//Modelos
use Modules\Panaderias\Model\Circuitos;
use Modules\Panaderias\Model\Ente;

use Modules\Panaderias\Model\Empresa;
use Modules\Direcciones\Model\Estados;
use Modules\Direcciones\Model\Municipio;
use Modules\Direcciones\Model\Parroquia;
use Modules\Direcciones\Model\Ciudades;
use Modules\Panaderias\Model\Despachos;

class DespachoCargaController extends Controller
{
    protected $titulo = 'Carga';
    
    public $meses = [		
		1 =>'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];

    public $js = [
        'Carga'
    ];
    
    public $css = [
        'Carga'
    ];

    public $librerias = [
        'ladda',
    ];

    public function index()
    {
        
        return $this->view('panaderias::DespachoCarga');
    }
    public function crear(Request $request)
    {
        set_time_limit(0);
		DB::beginTransaction();
		try {
			$mimes = [
			'text/csv',
			'application/vnd.ms-excel',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			];

			$ruta = public_path('archivos/');
			$archivo = $request->file('subir');

			$mime = $archivo->getClientMimeType();
			
			do {
				$nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
			} while (is_file($ruta . $nombre_archivo));
				
			$archivo->move($ruta, $nombre_archivo);

			chmod($ruta . $nombre_archivo, 0777);

			$excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();

	 		foreach ($excel as $key => $value) {
	 			
                 if($value->rif == '' || $value->codigo_sica == ''){
                    continue;
                 }
            /*  "rif"        
            "codigo_sica"  */
               $empresa = Empresa::where("rif", $value->rif)->where("codigo_sica", $value->codigo_sica)->first(); 

                $uno  = is_numeric($value->despacho_1_3) ? $value->despacho_1_3 : 0;
                $dos  = is_numeric($value->despacho_2_3) ? $value->despacho_2_3 : 0;
                $tres = is_numeric($value->despacho_3_3) ? $value->despacho_3_3 : 0;
            
               $resultado = $uno + $dos + $tres;
            
                if($resultado != 0 ){
                    $Despachos = Despachos::updateOrCreate(
                        [
                            "empresa_id" =>  $empresa->id,
                            "mes"=> $request->mes,
                            "ano"=> $request->ano
                        ],
                        [
                            "primera_entrega" => $uno,
                            "segunda_entrega" => $dos,
                            "tercera_entrega" => $tres,
                            "comentarios"	  => '',
                            'fecha_primera'   => $value->fecha_de_despacho_1_3,
                            'fecha_segunda'   => $value->fecha_de_despacho_2_3,
                            'fecha_tersera'   => $value->fecha_de_despacho_3_3

                        ]
				    );

                }  
                
	 		}
		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir'),];
    }
}