<?php 

namespace Modules\Panaderias\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController {
	protected $titulo = 'Panaderias';
	public $prefijo_ruta = '';

	public $app = 'admin';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Panaderias/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Panaderias/Assets/css',
	];
}