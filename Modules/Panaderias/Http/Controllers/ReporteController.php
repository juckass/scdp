<?php

namespace Modules\Panaderias\Http\Controllers;

//Controlador Padre
use Modules\Panaderias\Http\Controllers\Controller;

//Dependencias
use DB;
//use PDF;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Panaderias\Http\Requests\EmpresaRequest;

//Modelos
use Modules\Panaderias\Model\Empresa;
use Modules\Panaderias\Model\Despachos;
use Modules\Direcciones\Model\Estados;
use Modules\Direcciones\Model\Municipio;
use Modules\Direcciones\Model\Parroquia;
use Modules\Direcciones\Model\Ciudades;


class ReporteController extends Controller
{
    protected $titulo = 'Reporte';

    public $js = [
        'reporte.js'
    ];
    
    public $css = [
        'Reporte'
    ];

    public $meses = [       
        1 =>'Enero',
        'Febrero', 
        'Marzo',
        'Abril',
        'Mayo',              
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
    ];

    public $librerias = [
        
    ];

    public function index()
    {
        return $this->view('panaderias::reporte');
    }

    public function municipio()
    {
        return Municipio::where('estados_id', 6)->pluck('nombre','id');
    }

    public function imprimir(Request $request)
    {
        if ($request->isMethod('get')){
            return $this->data_imprimir($request, true);
        }

        $html = $this->data_imprimir($request, false);


       // $pdf = PDF::loadHTML($html);
        //return $pdf->stream('reporte.pdf');
        
        $pdf = new \mPDF('c', 'A4-L');
        $pdf->WriteHTML($this->data_imprimir($request, false));
        $pdf->Output();
    }

    protected function data_imprimir($request, $html)
    {
        //$tipoReporte = $request->tipoReporte;
        $reporte = ['html' => $html];
        
        $despachos = Despachos::select([      
            "empresa.id as empresaid",
            "empresa.rif",
            "empresa.codigo_sica",
            "empresa.razonsocial",
            "ente.nombre as ente",
            "empresa.despacho_mensual",
            "despachos.primera_entrega",
            "despachos.segunda_entrega",
            "despachos.tercera_entrega",
            "despachos.comentarios",
            'despachos.fecha_primera',
            'despachos.fecha_segunda',
            'despachos.fecha_tersera'
        ])
        ->where("despachos.mes", $request->mes)
        ->where('estado_empresa', '=',0)
        ->where("despachos.ano", $request->ano)   
        ->leftJoin('empresa', 'despachos.empresa_id','=','empresa.id')
        ->leftJoin('ente', 'ente.id','=','empresa.ente_id')
        ->leftJoin('municipio', 'empresa.municipio_id', '=', 'municipio.id');
        if($request->despachos == 1){
            if($request->falta == 0){
                $despachos->where("despachos.primera_entrega", ">", 0 );
            }else{
                $despachos->where("despachos.primera_entrega", 0);
            }
        } 
        if($request->despachos == 2){
            if($request->falta == 0){
                $despachos->where("despachos.segunda_entrega", ">", 0 );
            }else{
                $despachos->where("despachos.segunda_entrega", 0);
            }
        }
        if($request->despachos == 3){
            if($request->falta == 0){
                $despachos->where("despachos.tercera_entrega", ">", 0 );
            }else{
                $despachos->where("despachos.tercera_entrega", 0);
            }
        }
        if($request->municipio != '' ){
            $despachos->where("empresa.municipio_id", $request->municipio);
        }

        $reporte['datos'] = $despachos->get();
        return view('panaderias::pdf.reporte', $reporte)->render();
    }
}