<?php

namespace Modules\Panaderias\Http\Controllers;

//Controlador Padre
use Modules\Panaderias\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Panaderias\Http\Requests\DespachosRequest;

//Modelos
use Modules\Panaderias\Model\Despachos;
use Modules\Panaderias\Model\Empresa;

class DespachosController extends Controller {
	protected $titulo = 'Despachos';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
	];
	public $js =[
		'Despachos'
	];
	public $css =[
		'Despachos'
	];

	public $meses = [		
		1 =>'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];
	

	public function index() {
		return $this->view('panaderias::Despachos', [
			'Despachos' => new Despachos()
		]);
	}

	public function nuevo() {
		$Despachos = new Despachos();
		return $this->view('panaderias::Despachos', [
			'layouts' => 'admin::layouts.popup',
			'Despachos' => $Despachos
		]);
	}

	public function cambiar(Request $request, $id = 0) {
		$Despachos = Despachos::find($id);
		return $this->view('panaderias::Despachos', [
			'layouts' => 'admin::layouts.popup',
			'Despachos' => $Despachos
		]);
	}

	public function buscar(Request $request, $id = 0){
		
		//"","mes","ano"
		$Despachos = Despachos::where('empresa_id', $request->empresa_id )
		->where('mes', $request->mes)
		->where('ano', $request->ano)->first();

		$empresa = Empresa::select([
            'id',
			'rif',
			'codigo_sica',
			'razonsocial',
			'despacho_mensual'
       	])->where('id', $request->empresa_id)->first();

		return [
			'despacho' => $Despachos,
			'empresa'  =>  $empresa
		];
	}

	public function guardar(DespachosRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			
			$Despachos = Despachos::updateOrCreate(
					[
					 "empresa_id" => $request->empresa_id,
					 "mes"=> $request->mes,
					 "ano"=> $request->ano],
					[
						"primera_entrega" => $request->primera_entrega,
						"segunda_entrega" => $request->segunda_entrega,
						"tercera_entrega" => $request->tercera_entrega,

						"fecha_primera" => $request->fecha_primera,
						"fecha_segunda" => $request->fecha_segunda,
						"fecha_tersera" => $request->fecha_tersera,

						"comentarios"	  => $request->comentarios			
					]
				);

		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return [
			'id' => $Despachos->id, 
			'texto' => $Despachos->nombre, 
			's' => 's', 
			'msj' => trans('controller.incluir')
		];
	}

	public function eliminar(Request $request, $id = 0){
		try{
			Despachos::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function restaurar(Request $request, $id = 0) {
		try {
			Despachos::withTrashed()->find($id)->restore();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.restaurar')];
	}

	public function destruir(Request $request, $id = 0) {
		try {
			Despachos::withTrashed()->find($id)->forceDelete();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.destruir')];
	}


	public function datatable(Request $request)
    {
        $sql = Empresa::select([
            'id', 'circuitos_id', 'rif', 'codigo_sica','estado_empresa', 'razonsocial', 'nivel', 'ente_id',  'estados_id', 'despacho_mensual', 'deleted_at'
        ])
        ->where('estado_empresa', '=',0);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}