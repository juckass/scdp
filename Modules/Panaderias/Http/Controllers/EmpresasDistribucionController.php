<?php

namespace Modules\Panaderias\Http\Controllers;

//Controlador Padre
use Modules\Panaderias\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Panaderias\Http\Requests\EmpresasDistribucionRequest;

//Modelos
use Modules\Panaderias\Model\EmpresasDistribucion;

class EmpresasDistribucionController extends Controller {
	protected $titulo = 'Empresas Distribucion';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];
	public $js =[
		'EmpresasDistribucion'
	];
	public $css =[
		'EmpresasDistribucion'
	];

	public function index() {
		return $this->view('panaderias::EmpresasDistribucion', [
			'EmpresasDistribucion' => new EmpresasDistribucion()
		]);
	}

	public function nuevo() {
		$EmpresasDistribucion = new EmpresasDistribucion();
		return $this->view('panaderias::EmpresasDistribucion', [
			'layouts' => 'admin::layouts.popup',
			'EmpresasDistribucion' => $EmpresasDistribucion
		]);
	}

	public function cambiar(Request $request, $id = 0) {
		$EmpresasDistribucion = EmpresasDistribucion::find($id);
		return $this->view('panaderias::EmpresasDistribucion', [
			'layouts' => 'admin::layouts.popup',
			'EmpresasDistribucion' => $EmpresasDistribucion
		]);
	}

	public function buscar(Request $request, $id = 0){
		if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')){
			$EmpresasDistribucion = EmpresasDistribucion::withTrashed()->find($id);
		}else{
			$EmpresasDistribucion = EmpresasDistribucion::find($id);
		}
		
		if ($EmpresasDistribucion){
			return array_merge($EmpresasDistribucion->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function guardar(EmpresasDistribucionRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			if ($id === 0){
				$EmpresasDistribucion = EmpresasDistribucion::create($request->all());
			}else{
				$EmpresasDistribucion = EmpresasDistribucion::find($id);
				$EmpresasDistribucion->fill($request->all());
				$EmpresasDistribucion->save();
			}
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return [
			'id' => $EmpresasDistribucion->id, 
			'texto' => $EmpresasDistribucion->nombre, 
			's' => 's', 
			'msj' => trans('controller.incluir')
		];
	}

	public function eliminar(Request $request, $id = 0){
		try{
			EmpresasDistribucion::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function restaurar(Request $request, $id = 0) {
		try {
			EmpresasDistribucion::withTrashed()->find($id)->restore();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.restaurar')];
	}

	public function destruir(Request $request, $id = 0) {
		try {
			EmpresasDistribucion::withTrashed()->find($id)->forceDelete();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.destruir')];
	}

	public function datatable(Request $request){
		$sql = EmpresasDistribucion::select([
			'id', 'nombre', 'abastecimiento', 'deleted_at'
		]);

		if ($request->verSoloEliminados == 'true'){
			$sql->onlyTrashed();
		}elseif ($request->verEliminados == 'true'){
			$sql->withTrashed();
		}
		
		return Datatables::of($sql)
			->setRowId('id')
			->setRowClass(function ($registro) {
				return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
			})
			->make(true);
	}
}