<?php

namespace Modules\Panaderias\Http\Controllers;

//Controlador Padre
use Modules\Panaderias\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Panaderias\Http\Requests\EnteRequest;

//Modelos
use Modules\Panaderias\Model\Despachos;
use Modules\Panaderias\Model\Empresa;
use Modules\Panaderias\Model\EmpresasDistribucion;

class EscritorioController extends Controller
{
    protected $titulo = 'Escritorio';

    public $js;
    
    public $css = [
        'graficas.css'
    ];

    public $meses = [		
		1 =>'Enero',
		'Febrero', 
		'Marzo',
		'Abril',
		'Mayo',				 
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];
  
    public $librerias = [
        'datatables', 
        'highcharts',
		'highcharts-drilldown',
		'highcharts-3d',
        'highmaps'
    ];

    public function index()
    {  
      $this->js = [
        'graficas.js'
      ];

        $query = Empresa::select(['*'])->get();

        $activada            = clone $query;
        $_activada           = $activada ->where('estado_empresa', 0)->count();

        $bloqueda            = clone $query;
        $_bloqueada          = $bloqueda ->where('estado_empresa', 1)->count();

        $cerrada             = clone $query;
        $_cerrada            = $cerrada ->where('estado_empresa', 2)->count();

        $inactiva             = clone $query;
        $_inactiva            = $inactiva ->where('estado_empresa', 3)->count();

        $total               = $_activada + $_bloqueada + $_cerrada + $_inactiva;

        //dd($_cerrada);

        /*  0 => 'Activa',
        1 => 'Bloqueada',
        2 => 'Cerrada',
        3 => 'Inactivo' */

        return $this->view('panaderias::Escritorio',[
            'activada' => $_activada,
            'bloqueda' => $_bloqueada,
            'cerrada'  => $_cerrada,
            'inactiva'  => $_inactiva,
            'total'    => $total


        ]);
    }

    public function municipios($id = 0, $mes = 0, $ano = 0, $quien = '')
    {   
        $this->js = [
            'popup-municipio.js'
        ];
        return $this->view('panaderias::municipios-popup', [
            'layouts' => 'admin::layouts.popup2',
            'id' => $id,
            'ano'=> $ano,
            'mes'=> $mes,
            'quien' => $quien
        ]);
    } 



    public function tablaemprasas(Request $request){
    
        $_empresas = Empresa::select([
            DB::raw('Count(empresa.id) as empresas'),
            'municipio.nombre',
            'municipio.id' 
        ])
         ->leftJoin('municipio', 'empresa.municipio_id', '=', 'municipio.id')
         ->groupby('municipio.nombre')
         ->groupby('municipio.id')
         ->orderBy('municipio.nombre', 'asc')
         ->where('empresa.estado_empresa', '=',0)
         ->get();
        
        $empresas=[];

        foreach ($_empresas as $key => $empresa) {
           $empresas[str_slug($empresa->nombre)] = [
               'municipio_id' => $empresa->id,
               'municipio' => $empresa->nombre,
               'empresas'  => $empresa->empresas
           ];
        }
    
        
        $despachos = Despachos::select([      
          'municipio.nombre',
           DB::raw('Count(despachos.empresa_id) as despacho'),
        ])
        ->where('empresa.estados_id', 6)
        ->where("despachos.mes", $request->mes)
        ->where("despachos.ano", $request->ano)   
        ->leftJoin('empresa', 'despachos.empresa_id','=','empresa.id')
        ->leftJoin('municipio', 'empresa.municipio_id', '=', 'municipio.id')
        ->where('empresa.estado_empresa', '=',0)
        ->groupby('municipio.nombre')->get();
        

        if(count($despachos->toArray()) == 0) {
            foreach ($despachos as $key => $despacho) {

                $empresas[str_slug($despacho->nombre)]['despacho'] = 0;
                $empresas[str_slug($despacho->nombre)]['falta'] = $empresas[str_slug($despacho->nombre)]['empresas'] ;
            }   
        }else{
            foreach ($despachos as $key => $despacho) {
                $_despacho =  $despacho->despacho == '' ? 0 : $despacho->despacho;

                $empresas[str_slug($despacho->nombre)]['despacho'] = $_despacho;
                $empresas[str_slug($despacho->nombre)]['falta'] = 
                    $empresas[str_slug($despacho->nombre)]['empresas'] -
                    $_despacho;
            }   
        }
        /*------------------------------------------------------------------------------------*/    
        
      $_empresa_entes = Empresa::select([
            'ente.nombre',
            'ente.id as ente',
            DB::raw('Count(empresa.id) as empresas'),
        ])
        ->leftJoin('ente', 'ente.id','=','empresa.ente_id')
        ->groupby('ente.nombre')
        ->groupby('ente.id')
        ->where('empresa.estado_empresa', '=',0)
        ->get();

      $empresa_entes = [];

        foreach ($_empresa_entes as $key => $empresa_ente) {
            $empresa_entes[str_slug($empresa_ente->nombre)]=[
                'ente_id' => $empresa_ente->ente,
                'ente' => $empresa_ente->nombre,
                'empresas' => $empresa_ente->empresas,
            ];
        }

            
     $despachos_entes = Despachos::select([      
            'ente.nombre',
           DB::raw('Count(despachos.empresa_id) as despacho'),
        ])
        ->where('empresa.estados_id', 6)
        ->where("despachos.mes", $request->mes)
        ->where("despachos.ano", $request->ano)   
        ->leftJoin('empresa', 'despachos.empresa_id','=','empresa.id')
        ->leftJoin('ente', 'ente.id','=','empresa.ente_id')
        ->groupby('ente.nombre')
        ->where('empresa.estado_empresa', '=',0)
        ->get();
        
        foreach ($despachos_entes as $key => $despacho_ente) {
           $empresa_entes[str_slug($despacho_ente->nombre)]['despacho'] = $despacho_ente->despacho;
           $empresa_entes[str_slug($despacho_ente->nombre)]['falta'] = $empresa_entes[str_slug($despacho_ente->nombre)]['empresas'] - $despacho_ente->despacho;
        }

        return [
            "empresas" => $empresas,
            "entes"    => $empresa_entes
        ];
    }

    public function tablassaco(Request $request){

       /* alimentos bolivar : heres, sucre, cedeño, angostura
        mocasa: */

       $_centros  = EmpresasDistribucion::all();

       $municipio_alimento = [ '70', '201', '333', '72' ];
       $query = Despachos::select([
            DB::raw('Sum((despachos.primera_entrega + despachos.segunda_entrega + despachos.tercera_entrega )) as recibida'),
        ])
        ->where('empresa.estados_id', 6)
        ->where("despachos.mes", $request->mes)
        ->where("despachos.ano", $request->ano)
        ->where('empresa.estado_empresa', '=',0)
        ->leftJoin('empresa', 'despachos.empresa_id','=','empresa.id')
        ->leftJoin('municipio', 'empresa.municipio_id', '=', 'municipio.id');
       
       
        $_query = clone $query;
        $alimentos_bolivar = $query->whereIn('empresa.municipio_id', $municipio_alimento)->first();
        $mocasa = $_query->whereNotIn('empresa.municipio_id', $municipio_alimento)->first();

        $centros = [];
        foreach ($_centros as $key => $centro) {
            
        
            if(str_slug($centro->nombre) == "alimnetos-bolivar"){
                $recibida_sacos =  $alimentos_bolivar->recibida;
                $falta_saco =  $centro->abastecimiento -  $alimentos_bolivar->recibida;

            }else{
                $recibida_sacos =  $mocasa->recibida;
                $falta_saco =  $centro->abastecimiento -  $mocasa->recibida;
            }

            $abastecimiento_toneladas = ($centro->abastecimiento * 45)/1000;
            $recibida_toneladas =  ($recibida_sacos * 45)/1000;
            $falta_toneladas = ($falta_saco * 45)/1000;

            $centros[str_slug($centro->nombre)] = [
                'centro'          => $centro->nombre,
                'abastecimiento'  => $centro->abastecimiento,
                'recibida_sacos'  => $recibida_sacos,
                'falta_sacos'     => $falta_saco,
                'abastecimiento_toneladas'  => $abastecimiento_toneladas,
                'recibida_toneladas'  =>  $recibida_toneladas,
                'falta_toneladas'  =>  $falta_toneladas,

            ];
        }
        
        
        return [
            'centros' => $centros
        ];
    }
    
    public function graficaemprasas(Request $request){
        
        $empresas=[];
       
        // cantidad de empresas
        $_empresas = Empresa::select([
            DB::raw('Count(empresa.id) as empresas'),
            'municipio.nombre'
        ])
           ->leftJoin('municipio', 'empresa.municipio_id', '=', 'municipio.id')
           ->groupby('municipio.nombre')
           ->where('empresa.estado_empresa', '=',0)
           ->orderBy('municipio.nombre', 'asc')
        ->get();

        //listar las categorias 
        $categorias=[];
        foreach ($_empresas as $key => $empresa) {           
          $categorias[] = $empresa->nombre;
        }
        $_datas = [];
        foreach ($_empresas as $key => $empresa) {           
            $_datas[] = $empresa->empresas;
        }

        $empresas['Empresas del estado'] = [
            'name' => 'Total Empresas del Estado'
        ];
 
        foreach ($_datas as $key => $data) {           
             $empresas['Empresas del estado']['data'][] = intval($data) ;
        }
        

        /*---------------------------------------------------------------*/
        //Empresas Despachadas
        
        $despachos = Despachos::select([      
          'municipio.nombre',
           DB::raw('Count(despachos.empresa_id) as despacho'),
        ])
        ->where('empresa.estados_id', 6)
        ->where("despachos.mes", $request->mes)
        ->where("despachos.ano", $request->ano)   
        ->leftJoin('empresa', 'despachos.empresa_id','=','empresa.id')
        ->leftJoin('municipio', 'empresa.municipio_id', '=', 'municipio.id')
        ->where('empresa.estado_empresa', '=',0)
        ->groupby('municipio.nombre')->get();
        
        //total despachado
        $empresas['Total Despachado'] = [
            'name' => 'Total Despachado'
        ];

        $datas = [];

        foreach ($despachos as $key => $despacho ) {           
            $datas[] = $despacho->despacho;
        };

        $empresas['Total Faltante'] = [
            'name' => 'Total Faltante'
        ];
        
        foreach ($datas as $key => $data) {           
             $empresas['Total Despachado']['data'][] = intval($data);
             $empresas['Total Faltante']['data'][] =  intval($_datas[$key] - $data);
        };
    
        $empresas = array_values($empresas);
        return[
            'categories' =>  $categorias,
            'series'     =>   $empresas
        ];

    }
    
    public function empreasas(Request $request, $id = 0, $quien ='')
    {
        $sql = Empresa::select([
            "empresa.id",
            "empresa.rif",
            "empresa.codigo_sica",
            "empresa.razonsocial",
            'ente.nombre',
            "empresa.telefonos",
        ])
         ->leftJoin('ente', 'ente.id','=','empresa.ente_id')
         ->where('empresa.estado_empresa', '=',0);
        if($quien == 'entes'){
            $sql->where('empresa.ente_id', $id);
        }else{
            $sql->where('empresa.municipio_id', $id);
        }
            
        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function informacion(Request $request){
      
      $empresa = Empresa::find($request->empresa_id);
      $empresa->ente;
      $empresa->estados;
      $empresa->municipio;
      $empresa->ciudades;
      $empresa->circuitos;
      
      return $empresa;
    }

    public function despachos(Request $request, $id = 0)
    {
        
        $sql = Despachos::select([
            "despachos.ano",
            "despachos.mes",
            "despachos.primera_entrega",
            "despachos.segunda_entrega",
            "despachos.tercera_entrega",
            //"comentarios",
            'despachos.fecha_primera',
            'despachos.fecha_segunda',
            'despachos.fecha_tersera'
        ])
        ->where('despachos.empresa_id', $id);

       
        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->addColumn('mes', function ($sql) {
                return $this->meses[$sql->mes];
            })
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
             ->make(true);
    }
    
}