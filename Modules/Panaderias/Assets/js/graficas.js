var aplicacion, $form, tabla, mes_str = [];
$(function() {

    control($('#mes').val(), $('#ano').val());

    $('#mes, #ano ').on('change', function() {
        control($('#mes').val(), $('#ano').val());
    });


    $('#tabla_municipios').on('click', 'tr', function() {
        var $municipio_id = $(this).data('municipio');
        super_popup($municipio_id, 'municipios', $('#mes').val(), $('#ano').val());
    });

    $('#tabla_panaderias_clap').on('click', 'tr', function() {
        var $ente_id = $(this).data('ente-id');
        super_popup($ente_id, 'entes', $('#mes').val(), $('#ano').val());
    });
});
//funcion que ejecuta todas las funciones
function control($mes, $ano) {

    tabla_emprasas($mes, $ano);

    tabla_sacos($mes, $ano);

    graficas_empresas($mes, $ano);

}

function tabla_emprasas($mes, $ano) {
    mes_str = [
        'mes',
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
    ];

    $('.mes_str').html('');
    $('.mes_str').append(mes_str[$mes]);

    $.ajax({
        url: $url + 'tablaemprasas',
        type: 'POST',
        data: {
            'mes': $mes,
            'ano': $ano
        },
        success: function(r) {
            $('#tabla_panaderias_clap').html("");
            $('#tabla_municipios').html("");

            var cantidad = 0;
            var despachada = 0;
            var falta = 0;

            for (var i in r.empresas) {

                /*if (r.empresas[i].municipio === null) {
                    continue;
                }*/
                $despacho = 0;
                if (r.empresas[i].despacho != undefined) {
                    $despacho = r.empresas[i].despacho;
                }

                $falta = 0;
                if (r.empresas[i].falta != undefined) {

                    $falta = r.empresas[i].falta;

                }else{
                    
                    $falta =  r.empresas[i].empresas;
                }

              
                $('#tabla_municipios').append(
                    '<tr data-municipio = ' + r.empresas[i].municipio_id + '><td>' + r.empresas[i].municipio +
                    '</td><td>' + r.empresas[i].empresas +
                    '</td><td>' + $despacho +
                    '</td><td>' + $falta +
                    '</td></tr>'
                );

                cantidad = cantidad + parseInt(r.empresas[i].empresas);
                despachada = despachada + parseInt($despacho);
                falta = falta + parseInt($falta);
            }

            $('#tabla_municipios').append(
                '<tr><td>' + 'Total' +
                '</td><td>' + parseInt(cantidad) +
                '</td><td>' + parseInt(despachada) +
                '</td><td>' + parseInt(falta) +
                '</td></tr>'
            );


            var cantidad = 0;
            var despachada = 0;
            var falta = 0;

            for (var i in r.entes) {

                /*if (r.entes[i].nombre === null) {
                    continue;
                }*/
                $empresa = 0;
                if (r.entes[i].empresas != undefined) {
                    $empresa = r.entes[i].empresas;
                }

                $despacho = 0;
                if (r.entes[i].despacho != undefined) {
                    $despacho = r.entes[i].despacho;
                }

                $_falta = 0;
                if (r.entes[i].falta != undefined) {
                    $_falta = r.entes[i].falta;
                }

                $('#tabla_panaderias_clap').append(
                    '<tr data-ente-id = ' + r.entes[i].ente_id + '><td>' + r.entes[i].ente +
                    '</td><td>' + $empresa +
                    '</td><td>' + $despacho +
                    '</td><td>' + $_falta +
                    '</td></tr>'
                );

                cantidad = cantidad + parseInt($empresa);
                despachada = despachada + parseInt($despacho);
                falta = falta + parseInt($_falta);
            }

            $('#tabla_panaderias_clap').append(
                '<tr><td>' + 'Total' +
                '</td><td>' + cantidad +
                '</td><td>' + despachada +
                '</td><td>' + falta +
                '</td></tr>'
            );

            /*
            $('tr', '#tabla_municipios').on('click', function() {
                var $municipio = $(this).data('municipio');
                console.log($municipio);


                var id = $municipio;
            });
            */
        }
    });

}

function tabla_sacos($mes, $ano) {

    $.ajax({
        url: $url + 'tablassaco',
        type: 'POST',
        data: {
            'mes': $mes,
            'ano': $ano
        },
        success: function(r) {
            $('#tabla_sacos').html("");

            var total_abastecimiento_sacos = 0;
            var total_falta_sacos = 0;
            var total_recibida_sacos = 0;

            var total_abastecimiento_toneladas = 0;
            var total_falta_toneladas = 0;
            var total_recibida_toneladas = 0;

            for (var i in r.centros) {
                $('#tabla_sacos').append(
                    '<tr><td> ' + r.centros[i].centro +
                    '</td><td>' + r.centros[i].abastecimiento +
                    '</td><td>' + r.centros[i].falta_sacos +
                    '</td><td>' + r.centros[i].recibida_sacos +

                    '</td><td>' + parseFloat(r.centros[i].abastecimiento_toneladas).toFixed(2) +
                    '</td><td>' + parseFloat(r.centros[i].falta_toneladas).toFixed(2) +
                    '</td><td>' + parseFloat(r.centros[i].recibida_toneladas).toFixed(2) +
                    '</td></tr>'
                );
                total_abastecimiento_sacos = total_abastecimiento_sacos + parseInt(r.centros[i].abastecimiento);
                total_falta_sacos = total_falta_sacos + r.centros[i].falta_sacos;
                total_recibida_sacos = total_recibida_sacos + parseInt(r.centros[i].recibida_sacos);

                total_abastecimiento_toneladas = total_abastecimiento_toneladas + parseInt(r.centros[i].abastecimiento_toneladas);

                total_falta_toneladas = total_falta_toneladas + parseInt(r.centros[i].falta_toneladas);

                total_recibida_toneladas = total_recibida_toneladas + parseInt(r.centros[i].recibida_toneladas);
            }


            $('#tabla_sacos').append(
                '<tr><td>' + 'Total' +
                '</td><td>' + total_abastecimiento_sacos + ' (100%)' +
                '</td><td>' + total_falta_sacos + ' (' + parseFloat((total_falta_sacos * 100) / total_abastecimiento_sacos).toFixed(2) + '%)' +
                '</td><td>' + total_recibida_sacos + ' (' + parseFloat((total_recibida_sacos * 100) / total_abastecimiento_sacos).toFixed(2) + '%)' +

                '</td><td>' + total_abastecimiento_toneladas +
                '</td><td>' + total_falta_toneladas +
                '</td><td>' + total_recibida_toneladas +
                '</td></tr>'
            );

        }
    });

}

function graficas_empresas($mes, $ano) {

    var mes_str2 = [
        'mes',
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
    ];

    $.ajax({
        url: $url + 'graficaemprasas',
        type: 'POST',
        data: {
            'mes': $mes,
            'ano': $ano
        },
        success: function(r) {

            var chart = Highcharts.chart('grafica_empresa', {

                chart: {
                    type: 'column'
                },

                title: {
                    text: 'Panaderias Despachadas ' + mes_str2[$mes]
                },

                subtitle: {
                    text: ''
                },

                legend: {
                    align: 'right',
                    verticalAlign: 'middle',
                    layout: 'vertical'
                },

                xAxis: {
                    categories: r.categories,
                    labels: {
                        x: -10
                    }
                },

                yAxis: {
                    allowDecimals: false,
                    title: {
                        text: ''
                    }
                },

                series: r.series,

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },
                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }]
                }
            });

            $('#small').click(function() {
                chart.setSize(400, 300);
            });

            $('#large').click(function() {
                chart.setSize(600, 300);
            });

        }
    });

}

function super_popup($id, $adonde, $mes, $ano) {
    var direccion;
    direccion = $url + 'municipios/' + $id + '/' + $mes + '/' + $ano + '/' + $adonde;


    var win = window.open(direccion, 'municipios', 'height=500,width=1000,resizable=yes,scrollbars=yes');
    win.focus();
    return false

}