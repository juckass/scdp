var aplicacion, $form, tabla, $validar = false;
$(function() {
    aplicacion = new app('formulario', {
        'limpiar': function() {
            tabla.fnDraw();
        }
    });
    /*
        la suma de los 3 despachos tiene que ser igual o meno al despacho mensual de la empresa
        bloquear input  cuando sea mayor a 0 cuando busque.. que un super user pueda modificar- 
    */
    $('#primera_entrega').val(0);
    $('#segunda_entrega').val(0);
    $('#tercera_entrega').val(0);

    $("#fecha_primera", $form).datepicker({
        maxDate: "+0d"
    });
    //J095088111
    $("#fecha_segunda", $form).datepicker({
        maxDate: "+0d"
    });
    $("#fecha_tersera", $form).datepicker({
        maxDate: "+0d"
    });

    $form = aplicacion.form;

    tabla = datatable('#tabla', {
        ajax: $url + "datatable",
        columns: [
            { "data": "rif", "name": "rif" },
            { "data": "codigo_sica", "name": "codigo_sica" },
            { "data": "razonsocial", "name": "razonsocial" },
            { "data": "despacho_mensual", "name": "despacho_mensual" }
        ]
    });

    $('#tabla').on("click", "tbody tr", function() {
        buscar_despachos(this.id, $('#mes').val(), $('#ano').val());
    });

    $('#eliminar', '#botonera').remove();
    $('#buscar', '#botonera').remove();
    $('#limpiar', '#botonera').remove();
});

function buscar_despachos($emprsa_id, $mes, $ano) {

    if ($mes == "") {
        alert('Debe seleecinar un Mes')
        return false;
    }

    $.ajax({
        url: $url + 'buscar',
        type: 'POST',
        data: {
            'empresa_id': $emprsa_id,
            'mes': $mes,
            'ano': $ano
        },
        success: function(r) {
            $('#empresas .collapse').click();
            $('#despachos .expand').click();

            $("#rif").html('');
            $("#codigo_sica").html('');
            $("#razonsocial").html('');
            $("#despacho_mensual").html('');
            $("#empresa_id").val('');

            $("#empresa_id").val(r.empresa.id);

            $("#rif").append(r.empresa.rif);
            $("#codigo_sica").append(r.empresa.codigo_sica);
            $("#razonsocial").append(r.empresa.razonsocial);
            $("#despacho_mensual").append(r.empresa.despacho_mensual);

            if (r.despacho != null) {
                $('#primera_entrega').val(r.despacho.primera_entrega);
                $('#segunda_entrega').val(r.despacho.segunda_entrega);
                $('#tercera_entrega').val(r.despacho.tercera_entrega);
                $('#comentarios').val(r.despacho.comentarios);

                if (r.despacho.fecha_primera != null){
                     $("#fecha_primera").val(r.despacho.fecha_primera);
                }else{
                     $("#fecha_primera").val(''); 
                }
                if (r.despacho.fecha_segunda != null){
                     $("#fecha_primera").val(r.despacho.fecha_segunda);
                }else{
                     $("#fecha_primera").val(''); 
                }
                 if (r.despacho.fecha_segunda != null){
                     $("#fecha_primera").val(r.despacho.fecha_tersera);
                }else{
                     $("#fecha_primera").val(''); 
                }
                
                $("#fecha_tersera").val(r.despacho.fecha_tersera);

            } else {
                $('#primera_entrega').val(0);
                $('#segunda_entrega').val(0);
                $('#tercera_entrega').val(0);
                $("#fecha_primera").val('');
          
                $("#fecha_segunda").val('');
                $("#fecha_tersera").val('');
                $('#comentarios').val('');
            }
        }

    });


}