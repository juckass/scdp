var aplicacion, $form, tabla, $validar = false;
$(function() {
    aplicacion = new app('formulario', {
        'limpiar': function() {},
        'antes': function() {

        }
    });

    $("#subir").on('click', function(e) {
        e.preventDefault();
        $("#upload:hidden").trigger('click');
    });

    $('#eliminar', '#botonera').remove();
    $('#buscar', '#botonera').remove();
    $('#limpiar', '#botonera').remove();

    $("#guardar2").on('click', function() {

        if ($('#mes').val() == '') {
            alert('Selecione un Mes');
            return false;
        }

        var l = Ladda.create($("#subir").get(0));
        l.start();

        var options2 = {
            url: $url + 'crear',
            type: 'POST',
            data: $("#formulario").serialize(),
            success: function(r) {
                aviso(r);
                l.stop();
            }, 
            error: function() {
                l.stop();
            },
            complete: function(x, e, o) {
                ajaxComplete(x, e, o);

            }
        };

        $('#formulario').ajaxSubmit(options2);
    });



});