var aplicacion, $form, tabla, tabla2 = false,
    id2 = '',
    mes_str = [],
    $urlDtt = '',
    $v = false;
$(function() {
    tabla = datatable('#tabla', {
        ajax: dire + "/empresas/escritorio/datatable/" + id + '/' + quien,
        columns: [
            { "data": "rif", "name": "empresa.rif" },
            { "data": "codigo_sica", "name": "empresa.codigo_sica" },
            { "data": "razonsocial", "name": "empresa.razonsocial" },
            { "data": "nombre", "name": "ente.nombre" },

            { "data": "telefonos", "name": "empresa.telefonos" },
        ]
    });

    $('#tabla').on("click", "tbody tr", function() {
        buscar_info(this.id)
    });

});


function buscar_info($id) {
    $.ajax({
        url: dire + "/empresas/escritorio/informacion",
        type: 'POST',
        data: {
            'empresa_id': $id,
        },
        success: function(r) {

            despachos(r.id);
            $('#empresas .collapse').click();
            $('#infomacion .expand').click();
            $('#despachos .expand').click();

            $('#rif').html('').append(r.rif);
            $('#codigo_sica').html('').append(r.codigo_sica);
            $('#razonsocial').html('').append(r.razonsocial);

            $('#nivel').html('').append(r.nivel);

            $('#telefonos').html('').append(r.telefonos);
            $('#publica').html('').append(r.publica);
            if (r.ente_id != null) {
                $('#circuitos').html('').append(r.circuitos.nombre);
            }
            if (r.ente_id != null) {
                $('#ente').html('').append(r.ente.nombre);
            }

            if (r.estados_id != null) {
                $('#estado').html('').append(r.estados.nombre);
            }
            if (r.municipio_id != null) {
                $('#municipio').html('').append(r.municipio.nombre);
            }

            if (r.parroquia_id != null) {
                $('#parroquia').html('').append(r.parroquia.nombre);
            }

            if (r.ciudades_id != null) {
                $('#ciudad').html('').append(r.ciudades.nombre);
            }

            $('#direccion').html('').append(r.direccion);

        }
    });
}

function despachos($id) {
    $id2 = $id;
    $urlDtt = dire + "/empresas/escritorio/despachos/" + $id2 + '/' + quien;

    if (tabla2 !== false) {
        console.log(tabla2);
        $('#tabla_despachos').DataTable().ajax.url($urlDtt).load();
        //tabla2.fnDraw();
        return;
    }


    data_despachos();

}

function data_despachos() {
    tabla2 = datatable('#tabla_despachos', {
        ajax: $urlDtt,
        // ajax: urlDtt(),
        columns: [
            { "data": "primera_entrega", "name": "primera_entrega" },
            { "data": "fecha_primera", "name": "fecha_primera" },

            { "data": "segunda_entrega", "name": "fecha_segunda" },
            { "data": "fecha_segunda", "name": "fecha_segunda" },

            { "data": "tercera_entrega", "name": "tercera_entrega" },
            { "data": "fecha_tersera", "name": "fecha_tersera" },

            { "data": "mes", "name": "mes" },
            { "data": "ano", "name": "ano" },
        ]
    });
}

function urlDtt() {
    return $urlDtt;
}