var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.fnDraw();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{"data":"rif", "name":"rif"},
			{"data":"codigo_sica", "name":"codigo_sica"},
			{"data":"razonsocial", "name":"razonsocial"}
		]
	});
	
	$('#tabla').on("click","tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$('#estados_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'ciudades_id','ciudades');
		aplicacion.selectCascada($(this).val(), 'municipio_id','municipios');
	});
	
	$('#municipio_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'parroquia_id','parroquias');
	});

	$("#fecha_solicitud").datepicker();
	$("#fecha_activacion").datepicker();
});
