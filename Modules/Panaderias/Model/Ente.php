<?php

namespace Modules\Panaderias\Model;

use Modules\Admin\Model\Modelo;

class Ente extends modelo
{
    protected $table = 'ente';
    protected $fillable = ["nombre","slug"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Ente'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }
    public function empresas(){
		return $this->hasMany('Modules\Panaderias\Model\Empresa', 'ente_id');
	}

    
}