<?php
namespace Modules\Panaderias\Model;

use Modules\Admin\Model\Modelo;
use Carbon\Carbon;

class Despachos extends modelo
{
	protected $table = 'despachos';
    protected $fillable = [
		"empresa_id",
		"mes",
		"ano",
		"primera_entrega",
		"segunda_entrega",
		"tercera_entrega",
		"comentarios",
		'fecha_primera',
		'fecha_segunda',
		'fecha_tersera'
	];

   
    public function setFechaPrimeraAttribute($value)
    {
        // 2016-06-27
       
       if($value == ''){
            return false;
       }
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
          
            $formato = 'Y-m-d';
        }
        if (preg_match('/^\d{1,2}-\d{1,2}-\d{4}$/', $value)){
          
            $formato = 'd-m-Y';
        }
        $this->attributes['fecha_primera'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getFechaPrimeradAttribute($value){
        if($value == ''){
            return false;
        }
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaSegundaAttribute($value)
    {
        // 2016-06-27
       
       if($value == ''){
            return false;
       }
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
          
            $formato = 'Y-m-d';
        }
         if (preg_match('/^\d{1,2}-\d{1,2}-\d{4}$/', $value)){
          
            $formato = 'd-m-Y';
        }
        
        $this->attributes['fecha_segunda'] = Carbon::createFromFormat($formato, $value);
    }
    public function getFechaSegundaAttribute($value){
        if($value == ''){
            return false;
        }
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaTerseraAttribute($value)
    {
        // 2016-06-27
       
       if($value == ''){
            return false;
       }
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
          
            $formato = 'Y-m-d';
        }
        if (preg_match('/^\d{1,2}-\d{1,2}-\d{4}$/', $value)){
          
            $formato = 'd-m-Y';
        }
        $this->attributes['fecha_tersera'] = Carbon::createFromFormat($formato, $value);
    }  
    public function getFechaTerseraAttribute($value){
        if($value == ''){
            return false;
        }
        return Carbon::parse($value)->format('d/m/Y');
    }
   
}