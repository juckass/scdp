<?php
namespace Modules\Panaderias\Model;

use Modules\Admin\Model\Modelo;
use Modules\Direcciones\Model\Estados;
use Modules\Direcciones\Model\Ciudades;
use Modules\Direcciones\Model\Municipio;
use Modules\Direcciones\Model\Parroquia;

use Modules\Panaderias\Model\Circuitos;
use Modules\Panaderias\Model\Ente;
use Carbon\Carbon;

class Empresa extends modelo
{
	protected $table = 'empresa';
    protected $fillable = [
        "circuitos_id",
        "rif",
        "codigo_sica",
        "razonsocial",
        "nivel",
        "ente_id",
        "estados_id",
        "ciudades_id",
        "municipio_id",
        "parroquia_id",
        "direccion",
        
        "telefonos",
        "estado_empresa",
        "observacion",
        "despacho_mensual",
        
    ];
    protected $campos = [
        'circuitos_id'     => [
            'type'         => 'select',
            'label'        => 'Circuitos',
            'placeholder'  => '- Seleccione un Circuitos',
            'required'     => true,
            'url'          => 'empresas/definiciones/circuitos'
        ],
        'rif'              => [
            'type'         => 'text',
            'label'        => 'Rif',
            'placeholder'  => 'Rif del Empresa',
            'required'     => true
        ],
        'codigo_sica'      => [
            'type'         => 'number',
            'label'        => 'Código Sica',
            'placeholder'  => 'Codigo Sica del Empresa',
            'required'     => true
        ],
        'razonsocial'      => [
            'type'         => 'text',
            'label'        => 'Razón social',
            'placeholder'  => 'Razón social del Empresa',
            'required'     => true
        ],
        'nivel'            => [
            'type'         => 'number',
            'label'        => 'Nivel',
            'placeholder'  => 'Nivel del Empresa',
            'required'     => true
        ],
        'ente_id'          => [
            'type'         => 'select',
            'label'        => 'Ente',
            'placeholder'  => '- Seleccione un Ente',
            'required'     => true,
            'url'          => 'empresas/definiciones/entes'
        ],
        'estado_empresa'          => [
            'type'         => 'select',
            'label'        => 'estatus de la Empresa',
            'placeholder'  => '',
            'required'     => true,
            'options'    =>[
                0 => 'Activa',
                1 => 'Bloqueada',
                2 => 'Cerrada',
                3 => 'Inactivo'
            ] 

        ],
        'despacho_mensual' => [
            'type'         => 'number',
            'label'        => 'Despacho Mensual',
            'placeholder'  => 'Despacho Mensual del Empresa',
            'required'     => true
        ],
        'estados_id'       => [
            'type'         => 'select',
            'label'        => 'Estados',
            'placeholder'  => '- Seleccione un Estados',
            'url'          => '/direcciones/estados',
            'required'     => true
        ],
        'ciudades_id'      => [
            'type'         => 'select',
            'label'        => 'Ciudades',
            'placeholder'  => '- Seleccione un Ciudades',
            'url'          => '/direcciones/ciudades',
            'required'     => true
        ],
        'municipio_id'     => [
            'type'         => 'select',
            'label'        => 'Municipio',
            'placeholder'  => '- Seleccione un Municipio',
            'url'          => '/direcciones/municipio',
            'required'     => true
        ],
        'parroquia_id'     => [
            'type'         => 'select',
            'label'        => 'Parroquia',
            'placeholder'  => '- Seleccione un Parroquia',
            'url'          => '/direcciones/parroquia',
            'required'     => true
        ],
        'direccion'        => [
            'type'         => 'textarea',
            'label'        => 'Dirección',
            'placeholder'  => 'Dirección del Empresa',
            'cont_class'   => 'col-xs-12',
            'required'     => true
        ],
        "observacion"  => [
            'type'         => 'textarea',
            'label'        => 'Observacion',
            'placeholder'  => 'Observaciones',
            'cont_class'   => 'col-xs-12'
        ]
    ];

    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		$this->campos['circuitos_id']['options'] = Circuitos::pluck('nombre', 'id');
		$this->campos['ente_id']['options']      = Ente     ::pluck('nombre', 'id');
		$this->campos['estados_id']['options']   = Estados  ::pluck('nombre', 'id');

		/*$this->campos['ciudades_id']['options'] = Ciudades::pluck('nombre', 'id');
		$this->campos['municipio_id']['options'] = Municipio::pluck('nombre', 'id');
		$this->campos['parroquia_id']['options'] = Parroquia::pluck('nombre', 'id');*/
	}

    //cargado
    public function setFechaSolicitudAttribute($value)
    {
        // 2016-06-27
       
       if($value == ''){
            return false;
       }
        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
          
            $formato = 'Y-m-d';
        }
        
        $this->attributes['fecha_solicitud'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getFechaSolicitudAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setFechaActivacionAttribute($value)
    {
        // 2016-06-27
        if($value == ''){
           return false;
         }

        $formato = 'd/m/Y';
        if (preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $value)){
           
            $formato = 'Y-m-d';
        }
        
        $this->attributes['fecha_activacion'] = Carbon::createFromFormat($formato, $value);
    }
    
    public function getFechaActivacionAttribute($value){
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function ente(){
		return $this->belongsTo('Modules\Panaderias\Model\Ente', 'ente_id');
	}

    public function estados(){
		return $this->belongsTo('Modules\Direcciones\Model\Estados', 'estados_id');
	}

    public function municipio(){
		return $this->belongsTo('Modules\Direcciones\Model\Municipio', 'municipio_id');
	}
    public function parroquia(){
		return $this->belongsTo('Modules\Direcciones\Model\Parroquia', 'parroquia_id');
	}
    public function ciudades(){
		return $this->belongsTo('Modules\Direcciones\Model\Ciudades', 'ciudades_id');
	}
    public function circuitos(){
		return $this->belongsTo('Modules\Panaderias\Model\Circuitos', 'circuitos_id');
	}


}