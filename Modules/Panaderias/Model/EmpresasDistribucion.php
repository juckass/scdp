<?php
namespace Modules\Panaderias\Model;

use Modules\Admin\Model\Modelo;

class EmpresasDistribucion extends modelo
{
	protected $table = 'empresas_distribucion';
    protected $fillable = ["nombre","abastecimiento"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Empresas Distribucion',
        'required' => true
    ],
    'abastecimiento' => [
        'type' => 'number',
        'label' => 'Abastecimiento',
        'placeholder' => 'Abastecimiento del Empresas Distribucion',
        'required' => true
    ]
];

    public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);
		
	}
}