<?php

namespace Modules\Panaderias\Model;

use Modules\Admin\Model\Modelo;



class Circuitos extends modelo
{
    protected $table = 'circuitos';
    protected $fillable = ["nombre","slug"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Circuitos'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    public function empresas(){
		return $this->hasMany('Modules\Panaderias\Model\Empresa', 'circuitos_id');
	}
    
}