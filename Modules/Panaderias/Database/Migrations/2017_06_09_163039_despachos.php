<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Despachos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('despachos', function (Blueprint $table){
            
            $table->increments('id');
            $table->integer('empresa_id')->unsigned(); 
            $table->integer('mes')->unsigned(); 
            $table->integer('ano')->unsigned(); 
            $table->integer('primera_entrega')->unsigned()->nullable(); 
            $table->date('fecha_primera')->nullable();  
            $table->integer('segunda_entrega')->unsigned()->nullable(); 
            $table->date('fecha_segunda')->nullable(); 
            $table->integer('tercera_entrega')->unsigned()->nullable(); 
            $table->date('fecha_tersera')->nullable(); 
            $table->text('comentarios')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('empresa_id')
                ->references('id')->on('empresa')
                ->onDelete('cascade')->onUpdate('cascade');   

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despachos');
    }
}
