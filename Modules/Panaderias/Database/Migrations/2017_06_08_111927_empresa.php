<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Empresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('empresa', function (Blueprint $table){
            
            $table->increments('id');
            $table->integer('circuitos_id')->unsigned(); 
            $table->string('rif')->nullable();
            $table->string('codigo_sica')->nullable(); 
            $table->string('razonsocial')->nullable();;
            $table->integer('nivel')->unsigned()->nullable(); 
            $table->integer('ente_id')->unsigned()->nullable(); 
            $table->integer('estado_empresa')->unsigned()->default(0)->nullable(); 

            $table->integer('estados_id')->unsigned()->nullable();
            $table->integer('ciudades_id')->unsigned()->nullable();
            $table->integer('municipio_id')->unsigned()->nullable();
            $table->integer('parroquia_id')->unsigned()->nullable();
            $table->text('direccion')->nullable();
            $table->text('observacion')->nullable();

            $table->string('telefonos', 200)->nullable();
         
            $table->integer('despacho_mensual')->unsigned()->nullable(); 

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('circuitos_id')
                ->references('id')->on('circuitos')
                ->onDelete('cascade')->onUpdate('cascade');  

            $table->foreign('ente_id')
                ->references('id')->on('ente')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('estados_id')
                ->references('id')->on('estados')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('municipio_id')
                ->references('id')->on('municipio')
                ->onDelete('cascade')->onUpdate('cascade'); 

            $table->foreign('parroquia_id')
                ->references('id')->on('parroquia')
                ->onDelete('cascade')->onUpdate('cascade');
                
            $table->foreign('ciudades_id')
                ->references('id')->on('ciudades')
                ->onDelete('cascade')->onUpdate('cascade');    

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa');
    }
}
