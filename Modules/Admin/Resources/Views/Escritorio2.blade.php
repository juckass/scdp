<?php
$controller = app('Modules\Admin\Http\Controllers\Controller');
$controller->css[] = 'Escritorio.css';
$controller->js[] = 'Escritorio.js';

$data = $controller->_app();
extract($data);

$html['titulo'] = 'Inicio de Sesión';
?>
<!DOCTYPE html>
<!--[if IE 8]>    <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>    <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->
	<head>
		@include('admin::partials.head')
	</head>
	<body>
		@include('admin::partials.page-header')
		<div class="container">
			<div class="row" style="padding-top: 20px;">
				<div class="col-md-4 text-center"></div>
				@if ($controller->permisologia('claps'))
				<div class="col-md-4 text-center">
					<img class="img-responsive" style="height: 100px;" src="{{ asset('public/img/escritorio/clap.png') }}" />
					<br />
					<span style="border-bottom: solid 2px #FF5900;font-size: 20px;">SISTEMA DE ALIMENTOS</span>
					<br />
					<span>
						Control, planificaci&oacute;n y distribuci&oacute;n de la entrada y salida de los alimentos del estado Bol&iacute;var
					</span>
					<br />
					<br />
					<div class="btn-group" style="width: 100%;">
	                    <button type="button" class="btn dark" style="width: 50%; height: 50px;font-size: 10px;">Direccion de <br />informatica y sistemas</button>
	                    <a class="btn red-thunderbird" style="width: 50%; height: 50px;font-size: 24px;" href="{{ url('claps') }}" role="button">Ingresar</a>
	                </div>
					<!-- 
					<a class="btn btn-primary btn-lg" href="{{ url('ingresos/graficas/inventario') }}" role="button">Inventario de Alimentos</a>
					-->
				</div>
				@endif
				<div class="col-md-4 text-center"></div>
			</div>
			
		</div>
		
		@include('admin::partials.footer')

		<script type="text/javascript">
			$(".page-header-menu").remove();
			$(".dropdown-user").remove();
		</script>
	</body>
</html>