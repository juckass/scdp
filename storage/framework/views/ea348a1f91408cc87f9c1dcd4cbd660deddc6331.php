<?php $__env->startSection('content-top'); ?>

    <?php echo $__env->make('admin::partials.ubicacion', ['ubicacion' => ['Escritorio']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">

    <div class="col-md-12">
           
           <div class="row">
                <div class="col-md-4" >
                </div>
                <div class="col-md-4" >
                    <div class="dashboard-stat green">
                        <div class="visual">
                            <i class="fa fa-industry"></i>
                        </div>
                        <div class="details">
                            <div class="number"> <?php echo e($total); ?></div>
                            <div class="desc" > Total </div>
                        </div>
                            <a class="more" href="javascript:;" > Total de Empresas
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                    </div>
                </div>
                <div class="col-md-4" >
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="dashboard-stat  green-jungle ">
                        <div class="visual">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="details">
                            <div class="number"><?php echo e($activada); ?></div>
                            <div class="desc"> Activas </div>
                        </div>
                        <a class="more" href="javascript:;"> Todas las Empresas Activas
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="dashboard-stat  green-soft ">
                        <div class="visual">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="details">
                            <div class="number"><?php echo e($inactiva); ?></div>
                            <div class="desc"> Inactivas </div>
                        </div>
                        <a class="more" href="javascript:;"> Todas las Empresas Inactivas
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="dashboard-stat yellow">
                        <div class="visual">
                            <i class="fa fa-lock"></i>
                        </div>
                        <div class="details">
                            <div class="number"> <?php echo e($bloqueda); ?> </div>
                            <div class="desc"> Bloqueadas </div>
                        </div>
                        <a class="more"  href="javascript:;"> Todas las Empresas Bloqueadas
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="dashboard-stat grey-cascade">
                        <div class="visual">
                            <i class="fa fa-times"></i>
                        </div>
                            <div class="details">
                                <div class="number"><?php echo e($cerrada); ?>  </div>
                                <div class="desc"> Cerradas </div>
                            </div>
                            <a class="more"  href="javascript:;"> Todas las Empresas Cerradas
                                <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                    </div>
                </div>
                
            </div>
    </div>

        <?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]); ?>

            <?php echo e(Form::bsSelect('mes', $controller->meses, date('m'), [
                'label'      => 'Mes',
                'class_cont' => 'col-md-4 col-sm-6 col-xs-12',
                'required'   => 'required'
            ])); ?>

             
            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <label class="" for="mes">Año</label>
                <select name="ano" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="ano" required="required" class="form-control">
            

                    <?php for($i = 2017; $i <= date('Y') ; $i++): ?>
                        
                        <option value="<?php echo e($i); ?>" selected="selected"><?php echo e($i); ?></option>

                    <?php endfor; ?>

                </select>
            </div>
             
        <?php echo Form::close(); ?>

    </div>
    <div class="row"> 
        <div class="portlet box green" id="empresas">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-table"></i>Panaderias Despachadas En el Mes <span class="mes_str"></span></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <center><h3 class="panel-title"><b>Panaderias Despachadas por Municipios</b></h3></center> 
                    </div>
                    <div class="panel-body">
                        <center>
                            <table  class="table table-striped table-hover table-bordered tables-text">
                                <thead>
                                    <tr>
                                        <th style="width: 25%">Municipios</th>
                                        <th style="width: 25%">Cantidad</th>
                                        <th style="width: 30%">Despachada</th>
                                        <th style="width: 20%">Falta</th>	
                                    </tr>
                                </thead>
                                <tbody id="tabla_municipios">

                                </body>
                            </table>
                        </center>
                    </div>
                 </div>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <center><h3 class="panel-title"><b>Panaderias Despachadas por Entes</b></h3></center> 
                    </div>
                    <div class="panel-body">
                        <center>
                            <table  class="table table-striped table-hover table-bordered tables-text">
                                <thead>
                                    <tr>
                                        <th style="width: 25%">Entes</th>
                                        <th style="width: 25%">Cantidad</th>
                                        <th style="width: 30%">Despachada</th>
                                        <th style="width: 20%">Falta</th>	
                                    </tr>
                                </thead>
                                <tbody id="tabla_panaderias_clap">

                                </body>
                            </table>
                        </center>
                    </div>
                 </div>
            </div>
        </div>
	</div>

    <div class="row">
        <div class="portlet box green" id="empresas">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-table"></i>Harina Recibida En El Estado En El Mes <span class="mes_str"></span></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Por Sacos</h3>
                    </div>
                    <div class="panel-body">
                        <center>
                            <table  class="table table-striped table-hover table-bordered tables-text">
                                <thead>
                                    <tr>
                                        <th style="width: 14.28%">CENTRO</th>
                                        <th style="width: 14.2825%">ABASTECIMIENTO (SACOS)</th>
                                        <th style="width: 14.28%">POR RECIBIR (SACOS)</th>
                                        <th style="width: 14.28%">RECIBIDA (SACOS)</th>	
                                        <th style="width: 14.28%">ABASTECIMIENTO (TONELADAS)</th>
                                        <th style="width: 14.28%">POR RECIBIR (TONELADAS)</th>
                                        <th style="width: 14.28%">RECIBIDA (TONELADAS)</th>	
                                    </tr>
                                </thead>
                                <tbody id="tabla_sacos">

                                </body>
                            </table>
                        </center>
                    </div>
                </div>
    

            </div>
        </div>
	</div>

    <div class="row">
        <div class="portlet box green" id="empresas">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>Panaderias Despachadas En el Mes <span class="mes_str"></span></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <div id="grafica_empresa">  </div>
            </div>
        </div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make(isset($layouts) ? $layouts : 'admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>