'

<?php $__env->startSection('content-top'); ?>
	
	<?php echo $__env->make('admin::partials.ubicacion', ['ubicacion' => ['Despachos']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div id="botonera">
		<div class="btn-group btn-group-solid">
			<button id="guardar2" class="btn blue tooltips" data-original-title="Guardar Registro" data-placement="top" data-container="body">Guardar</button>
		</div>
	</div>
	<?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]); ?>

		<div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4>Cargar Archivo <small>Excel (.xls, .xlsx)</small></h4>
                <hr>
                <input id="upload" name="subir" type="file"/>
                <button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
                    <span class="ladda-label">
                        <i class="icon-arrow-right"></i> Carga archivo de empresas
                    </span>
                </button>
	        </div>
		</div>
	<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make(isset($layouts) ? $layouts : 'admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>