
<?php $__env->startSection('content'); ?>
	<div id="botonera">
		<!--
        <button id="buscar" class="btn green tooltips" data-container="body" data-placement="top" data-original-title="Inicio">
			<i class="fa fa-home"></i>
			<span class="visible-lg-inline visible-md-inline">Inicio</span>
		</button>
		<button id="atras" class="btn blue tooltips" data-container="body" data-placement="top" data-original-title="Atrás">
			<i class="fa fa-arrow-left"></i>
			<span class="visible-lg-inline visible-md-inline">Atr&aacute;s</span>
		</button>
        -->
		<button id="imprimir" class="btn btn-info tooltips" data-container="body" data-placement="top" data-original-title="<?php echo e(Lang::get('backend.btn_group.print.title')); ?>">
			<i class="fa fa-print"></i>
			<span class="visible-lg-inline visible-md-inline"><?php echo e(Lang::get('backend.btn_group.print.btn')); ?></span>
		</button>
	</div>
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="<?php echo e(url(\Config::get('admin.prefix'))); ?>">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Reportes<i class="fa fa-circle"></i></span>
		</li>
		<li>
			<span>Reporte</span>
		</li>
	</ul>
    
	<div class="row">
		<div class="portlet box green" id="">
            <div class="portlet-title">
                <div class="caption">
                    Filtros
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll" style="overflow: auto;">
               	<?php echo Form::open([
					'id' => 'formulario',
					'name' => 'formulario',
					'method' => 'post',
					'target' => '_blank',
					'url' => 'empresas/reporte/imprimir/pdf'
				]); ?>

			
			    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
		            <label class="" for="mes">Año</label>
		            <select name="ano" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="ano" required="required" class="form-control">
		        

		                <?php for($i = 2017; $i <= date('Y') ; $i++): ?>
		                    
		                    <option value="<?php echo e($i); ?>" selected="selected"><?php echo e($i); ?></option>

		                <?php endfor; ?>

		            </select>
		        </div>  

				<?php echo e(Form::bsSelect('mes', $controller->meses, date('m'), [
		            'label'      => 'Mes',
		            'required'   => 'required'
		        ])); ?>


		        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
		            <label class="" for="despachos">Despachos</label>
		            <select name="despachos" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="despachos" required="required" class="form-control">
		                <option value="0" selected="selected">Todos</option>
		                <option value="1" >Despacho 1</option>
		                <option value="2" >Despacho 2</option>
		                <option value="3" >Despacho 3</option>
		            </select>
		        </div>
		        <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
		            <label class="" for="despachos">Estatus</label>
		            <select name="falta" class_cont="col-lg-3 col-md-4 col-sm-6 col-xs-12" id="falta" required="required" class="form-control">
		                <option value="0" selected="selected">Despachados</option>
		                <option value="1" >Falta Por Despachar</option>       
		            </select>
		        </div>

		        <?php echo e(Form::bsSelect('municipio', $controller->municipio(),'', [
		            'label'      => 'Municipio',
		            'required'   => 'required'
		        ])); ?>

					
				<?php echo Form::close(); ?>

            </div>
        </div>

	</div>

	<div id="reportehtml"></div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>