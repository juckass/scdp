<?php $__env->startSection('content-top'); ?>
    <?php echo $__env->make('admin::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <?php echo $__env->make('admin::partials.ubicacion', ['ubicacion' => ['Municipio']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <?php echo $__env->make('admin::partials.modal-busqueda', [
        'titulo' => 'Buscar Municipio.',
        'columnas' => [
            'Estados' => '20',
		'Nombre' => '20',
		'Poblacion' => '20',
		'Opciones' => '20',
		'Path' => '20'
        ]
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]); ?>

            <?php echo $Municipio->generate(); ?>

        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make(isset($layouts) ? $layouts : 'admin::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>