<?php if(!$html): ?>
<?php echo $__env->make('panaderias::pdf.estilo_reporte', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>
<div id="reporteHtml" style="">
	<div class="header clearfix">
		<div id="logo" style="">	
			<div class="col-md-4 logo" id="img1">
				<img src="<?php echo e(url('public/img/logos/logo2.png')); ?>"  style="width: 4cm;margin-bottom: 15px;">
				<br>
			</div>
			<div class="col-md-4">
			
			</div>

			<div class="col-md-4 logo" id="img2">
				<img src="<?php echo e(url('public/img/logos/sunagro.png')); ?>" style="width: 4cm;" >
			</div>
		</div>
		
		<div>
			<h1>REPORTE DE DESPACHOS DE PANADERIAS</h1>
		</div>
		
		<div id="company" class="clearfix">
			<div style="margin-right: 35px;"><span>Fecha de impresi&oacute;n</span> <?php echo e(date('d/m/Y')); ?></div>
		</div>
	</header>
	<div class="main">  
		<table id="tabla">
			<thead>
				<tr>
					<th>Rif</th>

					<th>Código Sica</th>

					<th>Razón social</th>

					<th>Ente</th>

					<th>Despacho Men.</th>

					<th>Desp. 1</th>
					<th>Fecha</th>

					<th>Desp. 2</th>
					<th>Fecha</th>

					<th>Desp. 3</th>
					<th>Fecha</th>

					<th>Total</th>

					<th class="comentario">Comentarios</th>
					
				</tr>
			</thead>
			<tbody>

				<?php $__currentLoopData = $datos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reporte): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
					<tr data-empresa-id="<?php echo e($reporte->empresaid); ?>">
						<td class=""><?php echo e($reporte->rif); ?></td>
						<td class=""><?php echo e($reporte->codigo_sica); ?></td>
						<td class=""><?php echo e($reporte->razonsocial); ?></td>
						<td class=""><?php echo e($reporte->ente); ?></td>
						<td class=""><?php echo e($reporte->despacho_mensual); ?></td>

						<td class=""><?php echo e($reporte->primera_entrega); ?></td>

						<td class=""><?php echo e($reporte->fecha_primera); ?></td>

						<td class=""><?php echo e($reporte->segunda_entrega); ?></td>

						<td class=""><?php echo e($reporte->fecha_segunda); ?></td>

						<td class=""><?php echo e($reporte->tercera_entrega); ?></td>

						<td class=""><?php echo e($reporte->fecha_tersera); ?></td>

						<td class=""><?php echo e($reporte->primera_entrega +  $reporte->segunda_entrega +  $reporte->fecha_tersera); ?></td>
						
						<?php if($reporte->comentarios == ''): ?>
							<td class="comentario"></td>
						<?php else: ?>
							<td class="comentario"><i class="fa fa-comment-o" aria-hidden="true"></i></td>
						<?php endif; ?>
					</tr>

				<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
				<!-- <tr style="font-weight: bold;">
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>					
				</tr> -->
			</tbody>
		</table>	
	</div>
	
</div>